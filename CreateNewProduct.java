package com.test.api;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.io.*;
import java.math.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mozu.api.contracts.commerceruntime.discounts.AppliedLineItemProductDiscount;
import com.mozu.api.contracts.commerceruntime.fulfillment.Shipment;
import com.mozu.api.contracts.commerceruntime.payments.Payment;
import com.mozu.api.contracts.location.Location;
import com.mozu.api.contracts.location.LocationCollection;
import com.mozu.api.contracts.productadmin.ProductOption;
import com.mozu.api.contracts.content.Document;
import com.mozu.api.contracts.core.Measurement;
import com.mozu.api.contracts.productadmin.LocationInventory;
import com.mozu.api.contracts.productadmin.LocationInventoryAdjustment;
import com.mozu.api.contracts.productadmin.LocationInventoryCollection;
import com.mozu.api.contracts.productadmin.Product;
import com.mozu.api.contracts.productadmin.ProductCategory;
import com.mozu.api.contracts.productadmin.ProductCollection;
import com.mozu.api.contracts.productadmin.ProductCost;
import com.mozu.api.contracts.productadmin.ProductInCatalogInfo;
import com.mozu.api.contracts.productadmin.ProductInventoryInfo;
import com.mozu.api.contracts.productadmin.ProductLocalizedContent;
import com.mozu.api.contracts.productadmin.ProductLocalizedImage;
import com.mozu.api.contracts.productadmin.ProductLocalizedSEOContent;
import com.mozu.api.contracts.productadmin.ProductOptionValue;
import com.mozu.api.contracts.productadmin.ProductPrice;
import com.mozu.api.contracts.productadmin.ProductProperty;
import com.mozu.api.contracts.productadmin.ProductPropertyValue;
import com.mozu.api.contracts.productadmin.ProductPublishingInfo;
import com.mozu.api.contracts.productadmin.ProductSupplierInfo;
import com.mozu.api.contracts.productadmin.ProductVariation;
import com.mozu.api.contracts.productadmin.ProductVariationDeltaPrice;
import com.mozu.api.contracts.productadmin.ProductVariationPagedCollection;
import com.mozu.api.contracts.productruntime.ProductImage;
import com.mozu.api.resources.commerce.admin.LocationResource;
import com.mozu.api.resources.commerce.catalog.admin.ProductResource;
import com.mozu.api.resources.content.documentlists.DocumentResource;
import com.mozu.api.ApiContext;
import com.mozu.api.MozuApiContext;
import com.mozu.api.resources.commerce.catalog.admin.products.LocationInventoryResource;
import com.mozu.api.resources.commerce.catalog.admin.products.ProductVariationResource;
import com.mozu.api.contracts.productadmin.ProductType;
import com.mozu.api.resources.commerce.catalog.admin.products.ProductPropertyResource;
import com.mozu.api.resources.commerce.catalog.admin.attributedefinition.ProductTypeResource;

public class CreateNewProduct extends MozuInit 
{

	private ApiContext apiContext;
	ProductResource productResource = null;
	ProductVariationResource pvrChild = null;
	DocumentResource docresImage = null;
	LocationResource locresInventory = null;
	LocationInventoryResource locinvresInventory = null;
	ProductPropertyResource propRes = null;
	private String sSite = "";
	private String [] sLoc = null;
	ProductTypeResource prodTyRes = null;
	Product product = null;
	
	public CreateNewProduct()  
	{
		try 
		{  
			System.out.println("MozuProducts init();");			
			apiContext = super.getApiCont();
			
			productResource = new ProductResource(apiContext);
			pvrChild = new ProductVariationResource(apiContext);
			docresImage = new DocumentResource(apiContext);
			locresInventory = new LocationResource(apiContext);
			locinvresInventory = new LocationInventoryResource(apiContext);
            propRes = new ProductPropertyResource(apiContext);
            prodTyRes = new ProductTypeResource(apiContext); 
            
			
			SASS_refresh_locations sassloc = new SASS_refresh_locations(apiContext);
			sassloc.setSASSLocation();
			sLoc = sassloc.getSASSLocation();
			
			product = new Product();
			product.setProductCode("2203051473312");
			product.setProductTypeId(3);
			product.setProductUsage("Configurable");
			 
			List<String> lFflType = new ArrayList<String>();
			lFflType.add("DirectShip");
			product.setFulfillmentTypesSupported(lFflType);
			
			product.setMasterCatalogId(1);
			product.setIsValidForProductType(true);
			
			
			List<ProductInCatalogInfo> lProdCatInfo = new ArrayList<ProductInCatalogInfo>();
			ProductInCatalogInfo prodCatInfo = new ProductInCatalogInfo();
			prodCatInfo.setCatalogId(1);
			prodCatInfo.setIsActive(true);
			prodCatInfo.setIsContentOverridden(false);
			prodCatInfo.setIsPriceOverridden(false);
			prodCatInfo.setIsseoContentOverridden(false);
			ProductLocalizedContent plcLoc = new ProductLocalizedContent();
			plcLoc.setLocaleCode("en-US");
			plcLoc.setProductName("Women's Reserved Ivy Insulated Jacket");
			plcLoc.setProductShortDescription("It is a jacket");
			plcLoc.setProductFullDescription("Women's Reserved Ivy Insulated Jacket");	 
			//plcLoc.setProductImages();
			
			prodCatInfo.setContent(plcLoc);
			
			List<ProductCategory> lProdCat = new ArrayList<ProductCategory>();
			ProductCategory pcCat = new ProductCategory();
			pcCat.setCategoryId(1);
			lProdCat.add(pcCat);
			prodCatInfo.setProductCategories(lProdCat); 
			
			lProdCatInfo.add(prodCatInfo);
			product.setProductInCatalogs(lProdCatInfo);
			
			ProductPrice pp = new ProductPrice();
			pp.setMsrp(45.34);
			pp.setPrice(31.99);			
			product.setPrice(pp);
			
			ProductLocalizedSEOContent plcSeo = new ProductLocalizedSEOContent();
			plcSeo.setLocaleCode("en-US");
			plcSeo.setMetaTagTitle(plcLoc.getProductName());
			plcSeo.setSeoFriendlyUrl("test-friendly-url");
			plcSeo.setMetaTagKeywords("Women's Apparel");
			product.setSeoContent(plcSeo);
						
			product.setHasConfigurableOptions(true);
			product.setHasStandAloneOptions(false);
			product.setIsVariation(false);
			product.setIsTaxable(true);
			
			ProductInventoryInfo piiInfo = new ProductInventoryInfo();
			piiInfo.setManageStock(true);
			piiInfo.setOutOfStockBehavior("HIDEPRODUCT"); //DisplayMessage, AllowBackOrder
			product.setInventoryInfo(piiInfo);
			 
			product.setIsRecurring(false);
			
			ProductSupplierInfo psiSuppl = new ProductSupplierInfo();
			psiSuppl.setMfgPartNumber("Abc_123");
			ProductCost cost = new ProductCost();
			cost.setCost(21.85);
			cost.setIsoCurrencyCode("USD");
			psiSuppl.setCost(cost);
			product.setSupplierInfo(psiSuppl);
			
			product.setIsPackagedStandAlone(false);
			product.setStandAlonePackageType("Custom");
			ProductPublishingInfo ppiPub = new ProductPublishingInfo();
			ppiPub.setPublishedState("Live"); //Pending???			
			product.setPublishingInfo(ppiPub);
			
			product.setContent(plcLoc);
			
			Measurement mWght = new Measurement();
			mWght.setUnit("lbs");
			mWght.setValue(1.00);
			product.setPackageWeight(mWght);
			
			Measurement mLen = new Measurement();
			mLen.setUnit("in");
			mLen.setValue( 20.00 );
			product.setPackageLength(mLen);
			
			Measurement mHgt = new Measurement();
			mHgt.setUnit("in");
			mHgt.setValue( 30.00 );
			product.setPackageHeight(mHgt);
			
			Measurement mWdt = new Measurement();
			mWdt.setUnit("in");
			mWdt.setValue( 10.00 );
			product.setPackageWidth(mWdt);
			
			productResource.addProduct(product);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}

	public static void main(String[] args) 
	{
		CreateNewProduct getnew = new CreateNewProduct();
	}

}
