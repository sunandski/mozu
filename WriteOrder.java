package com.test.api;
import java.math.BigDecimal;
import java.util.Vector;

import rciutility.CallAs400SrvPgmSup;

public class WriteOrder extends CallAs400SrvPgmSup
{
    /**
     * ======================================================================
     * Constructor - write data in order header and detail file
     * ====================================================================== */
	public WriteOrder() 
	{
		setSrvPgmName("RCI", "MOORDSV");   
	}
	/**
	 * ======================================================================
	 * save Order Header Information
	 * ====================================================================== */
	public void saveOrderHdr(String sSite, String sOrdNum, String sCustNum
	 , String sBillComp, String sBillFName, String sBillLName, String sBillAddr1, String sBillAddr2
	 , String sBillCity, String sBillState, String sBillZip, String sBillCntry, String sBillHome
	 , String sBillMobile, String sBillWork
	 
	 , String sShipComp, String sShipFName, String sShipLName, String sShipAddr1, String sShipAddr2
	 , String sShipCity, String sShipState, String sShipZip, String sShipCntry, String sShipHome
	 , String sShipMobile, String sShipWork
	 
	 , String sShipMethod, String sShipCost, String sCreatedDate, String sIpAddr, String sLastModDt
	 , String sLastModBy, String sOrdSts, String sShipRes, String sTaxTot, String sSubtot
	 , String sTotal, String sTotalPaid
	 )
	{
		StringBuffer sbParam = new StringBuffer();
	    sbParam.append(setParamString(sSite, 10));   
	    sbParam.append(setParamString(sOrdNum, 10));
	    sbParam.append(setParamString(sCustNum, 10));
	    
	    sbParam.append(setParamString(sBillComp, 50));
	    sbParam.append(setParamString(sBillFName, 25));
	    sbParam.append(setParamString(sBillLName, 25));
	    sbParam.append(setParamString(sBillAddr1, 30));
	    sbParam.append(setParamString(sBillAddr2, 30));
	    sbParam.append(setParamString(sBillCity, 30));
	    sbParam.append(setParamString(sBillState, 2));
	    sbParam.append(setParamString(sBillZip, 10));
	    sbParam.append(setParamString(sBillCntry, 30));
	    sbParam.append(setParamString(sBillHome, 12));
	    sbParam.append(setParamString(sBillMobile, 12));
	    sbParam.append(setParamString(sBillWork, 12));
	    
	    sbParam.append(setParamString(sShipComp, 50));
	    sbParam.append(setParamString(sShipFName, 25));
	    sbParam.append(setParamString(sShipLName, 25));
	    sbParam.append(setParamString(sShipAddr1, 30));
	    sbParam.append(setParamString(sShipAddr2, 30));
	    sbParam.append(setParamString(sShipCity, 30));
	    sbParam.append(setParamString(sShipState, 2));
	    sbParam.append(setParamString(sShipZip, 10));
	    sbParam.append(setParamString(sShipCntry, 30));
	    sbParam.append(setParamString(sShipHome, 12));
	    sbParam.append(setParamString(sShipMobile, 12));
	    sbParam.append(setParamString(sShipWork, 12));
	    
	    sbParam.append(setParamString(sShipMethod, 30));
	    sbParam.append(setParamString(sShipCost, 20));
	    
	    sbParam.append(setParamString(sCreatedDate, 20));
	    sbParam.append(setParamString(sIpAddr, 20));
	    sbParam.append(setParamString(sLastModDt, 20));
	    sbParam.append(setParamString(sLastModBy, 30));
	    sbParam.append(setParamString(sOrdSts, 20));
	    sbParam.append(setParamString(sShipRes, 5));
	    
	    sbParam.append(setParamString(sTaxTot, 20));
	    sbParam.append(setParamString(sSubtot, 20));
	    sbParam.append(setParamString(sTotal, 20));
	    sbParam.append(setParamString(sTotalPaid, 20));
	    
	    // set class and store list
	    String [] sParam = new String[]{sbParam.toString()};
	    String sInp = sbParam.toString();
	    int [] iPrmLng = {sbParam.toString().length()};
	    runSrvPgmProc("SAVEORDHDR", sParam, iPrmLng);
	}
	
	/**
	 * ======================================================================
	 * save Order payments
	 * ====================================================================== */
	public void savePayment(String sSite, String sOrder, Vector vPays, Vector vType, Vector vCardType)
	{
		String [] sAmt = (String[]) vPays.toArray(new String[vPays.size()]);
		String [] sType = (String[]) vType.toArray(new String[vType.size()]);
		String [] sCardType = (String[]) vCardType.toArray(new String[vCardType.size()]);
		
		// set class and store list
		StringBuffer sbParam = new StringBuffer();
		for(int i=0; i < 20 ;i++)
		{
			if(i < sType.length)
			{ 
				sbParam.append(setParamString(sSite, 10));
				sbParam.append(setParamString(sOrder, 10));
				sbParam.append(setParamString(sAmt[i], 20));
				sbParam.append(setParamString(sType[i], 50));
				sbParam.append(setParamString(sCardType[i], 50));
			}	
			else{ sbParam.append(setParamString(" ", 10 + 10 + 20 + 50 + 50)); };
		}
		
	    String [] sParam = new String[]{sbParam.toString()};
	    String sInp = sbParam.toString();
	    int [] iPrmLng = {sbParam.toString().length()};	    
	    runSrvPgmProc("SAVEPAYS", sParam, iPrmLng);
	}
	
	/**
	 * ======================================================================
	 * save Order shipments
	 * ====================================================================== */
	public void saveShipments(String sSite, String sOrder, Vector vMeth, Vector vCost)
	{
		String [] sMeth = (String[]) vMeth.toArray(new String[vMeth.size()]);
		String [] sCost = (String[]) vCost.toArray(new String[vCost.size()]);
		
		// set class and store list
		StringBuffer sbParam = new StringBuffer();
		for(int i=0; i < 20 ;i++)
		{
			if(i < sMeth.length)
			{ 
				sbParam.append(setParamString(sSite, 10));
				sbParam.append(setParamString(sOrder, 10));
				sbParam.append(setParamString(sMeth[i], 50));
				sbParam.append(setParamString(sCost[i], 20));
			}	
			else{ sbParam.append(setParamString(" ", 10 + 10 + 50 + 20)); };
		}
		
	    String [] sParam = new String[]{sbParam.toString()};
	    String sInp = sbParam.toString();
	    int [] iPrmLng = {sbParam.toString().length()};	    
	    runSrvPgmProc("SAVESHIPS", sParam, iPrmLng);
	}
	
	/** ======================================================================
	 * save Order Header Information
	 * ====================================================================== */
	public void saveOrderDtl(String sSite, String sOrdNum, String sProd, String sQty
			, String sPrice, String sRet, String sExtRet, String sDisc, String sTotal)	
	{
		StringBuffer sbParam = new StringBuffer();
	    sbParam.append(setParamString(sSite, 10));   
	    sbParam.append(setParamString(sOrdNum, 10));
	    sbParam.append(setParamString(sProd, 50));
	    sbParam.append(setParamString(sQty, 20));
	    sbParam.append(setParamString(sPrice, 20));
	    sbParam.append(setParamString(sRet, 20));
	    sbParam.append(setParamString(sExtRet, 20));
	    sbParam.append(setParamString(sDisc, 20));
	    sbParam.append(setParamString(sTotal, 20));
	    
	 // set class and store list
	    String [] sParam = new String[]{sbParam.toString()};
	    String sInp = sbParam.toString();
	    int [] iPrmLng = {sbParam.toString().length()};
	    runSrvPgmProc("SAVEORDDTL", sParam, iPrmLng);
	}
	
	/** ======================================================================
	 * save Order Header Information
	 * ====================================================================== */
	public void saveItemDisc(String sSite, String sOrdNum, String sProd, Vector<String> vCode
			, Vector<String> vQty, Vector<String> vAmt)	
	{
		String [] sCode = (String[]) vCode.toArray(new String[vCode.size()]);
		String [] sQty = (String[]) vQty.toArray(new String[vQty.size()]);
		String [] sAmt = (String[]) vAmt.toArray(new String[vAmt.size()]);
		
		// set class and store list
		StringBuffer sbParam = new StringBuffer();
		for(int i=0; i < 20 ;i++)
		{
			if(i < sCode.length)
			{ 
				sbParam.append(setParamString(sSite, 10));
				sbParam.append(setParamString(sOrdNum, 10));
				sbParam.append(setParamString(sProd, 50));
				sbParam.append(setParamString(sCode[i], 50));
				sbParam.append(setParamString(sQty[i], 13));
				sbParam.append(setParamString(sAmt[i], 13));
			}	
			else{ sbParam.append(setParamString(" ", 10 + 10 + 50 + 50 + 13 + 13)); };
		}
		
	    String [] sParam = new String[]{sbParam.toString()};
	    int [] iPrmLng = {sbParam.toString().length()};	    
	    runSrvPgmProc("SAVEDISC", sParam, iPrmLng);
	}
	
	/** ======================================================================
	 * test 
	 * ====================================================================== */
	public static void main(String[] args) 
	{
		WriteOrder wrtord = new WriteOrder();
	}

}
