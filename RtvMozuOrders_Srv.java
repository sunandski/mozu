package com.test.api;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.io.*;
import java.math.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mozu.api.contracts.commerceruntime.discounts.AppliedLineItemProductDiscount;
import com.mozu.api.contracts.commerceruntime.fulfillment.Shipment;
import com.mozu.api.contracts.commerceruntime.orders.Order;
import com.mozu.api.contracts.commerceruntime.orders.OrderCollection;
import com.mozu.api.contracts.commerceruntime.payments.Payment;
import com.mozu.api.contracts.commerceruntime.orders.OrderItem;
import com.mozu.api.resources.commerce.OrderResource;
import com.mozu.api.ApiContext;
import com.mozu.api.MozuApiContext;

import org.joda.time.DateTime;
import org.json.*;

import rciutility.CallAs400SrvPgmSup;
import rciutility.ConnDataQueueObject;


public class RtvMozuOrders_Srv extends MozuInit 
{
	private String sFilter = "";
	private int iWait = -1;//15 * 60; //15 minutes	
	private String sSite = " ";
	private String sOrdNum = " ";
	
	private String sCustNum = " ";
	private String sBillComp = " ";
	private String sBillFName = " ";
	private String sBillLName = " ";
	private String sBillAddr1 = " ";
	private String sBillAddr2 = " ";
	private String sBillCity = " ";
	private String sBillState = " ";
	private String sBillZip = " ";
	private String sBillCntry = " ";
	private String sBillHome = " ";
	private String sBillMobile = " ";
	private String sBillWork = " ";
	
	private String sShipComp = " ";
	private String sShipFName = " ";
	private String sShipLName = " ";
	private String sShipAddr1 = " ";
	private String sShipAddr2 = " ";
	private String sShipCity = " ";
	private String sShipState = " ";
	private String sShipZip = " ";
	private String sShipCntry = " ";
	private String sShipHome = " ";
	private String sShipMobile = " ";
	private String sShipWork = " ";
	
	private String sShipMethod = " ";
	private String sShipCost = " ";	
	private String sCreatedDate = " ";	
	private String sIpAddr = " ";			
	private String sLastModDt = " "; 
	private String sOrdSts = " ";
	private String sLastModBy = " ";
	private String sShipRes = "N";	
	
	// tax rate 1,2,3 tax amt 1,2,3 ?????
	private String sTaxTot = " ";
	private String sSubtot = " ";
	private String sTotal = " ";
	private String sTotalPaid = " ";
	
	
	private String sItmPrc = " ";
	private String sItmProd = " ";
	private String sItmQty = " ";
	private String sItmRet = " ";
	private String sItmExtRet = " ";
	private String sItmDisc = " ";
	private String sItmTotal = " ";
	private Vector<String> vItmPickLoc = new Vector<String>();
	
	private WriteOrder wrtord = new WriteOrder();	
	private ApiContext apiContext;	
	ConnDataQueueObject dqOrders = new ConnDataQueueObject("TMPLIB","MOORDERS");
		
	
	/**
	 * ===================================================================
	 * Constructor - initialize connection via super class
	 * ===================================================================
	 */
	public RtvMozuOrders_Srv() 
	{
		try 
		{  
			init();
			apiContext = super.getApiCont();
			
			dqOrders.setWaitTime(iWait);
			getFilter();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}
	/** ===================================================================
	 * get filter
	 * ===================================================================*/
	private void getFilter()
	{
		String sData = null;
		// data queue
		while(!sFilter.equals("END"))
		{			
			sData = dqOrders.read();
			if(sData != null)
			{
				int i = Integer.parseInt(sData.substring(0,5).trim());
				if(i != 0)
				{ 
					iWait = i; 
					dqOrders.setWaitTime(i);    
				}
					
				String f = sData.substring(5);
				if(!f.equals("")){ sFilter = f; }					
			}
				
			// get orders
			if(!sFilter.trim().equals("END") && !sFilter.trim().equals("")) 
			{ 
				getOrdersByFilter(sFilter); 
			}
		}		
	}
	/** ===================================================================
	 * get order by filter
	 * ===================================================================*/
	private void getOrdersByFilter(String sFilter) 
	{	
		try
		{
			System.out.println("Filter = " + sFilter);
			
			OrderResource orderResource = new OrderResource(apiContext);
			OrderCollection orderCollection = orderResource.getOrders(0,50, null, sFilter, null, null, null);
			List<Order> orders = orderCollection.getItems();
			int i = 1;
			for (Order order : orders) 
			{	
				try
				{	
					getOrdHdr(order);
					System.out.println(i + ". " + sOrdNum);
					i++;
					getItems(order);
					// item list
			
		  } catch(Exception e)
		  { 
			  System.out.println(i + ". Error = " + e.getMessage());
		  }
		}
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	
	//===================================================================
	// get order header
	//===================================================================
	private void getOrdHdr(Order order) throws Exception
	{
		sSite = Integer.toString(order.getTenantId());
		sOrdNum = Integer.toString(order.getOrderNumber());	
		
		sCustNum = Integer.toString(order.getCustomerAccountId());
		sBillComp = order.getBillingInfo().getBillingContact().getCompanyOrOrganization();
		sBillFName = order.getBillingInfo().getBillingContact().getFirstName();
		sBillLName = order.getBillingInfo().getBillingContact().getLastNameOrSurname();
		sBillAddr1 = order.getBillingInfo().getBillingContact().getAddress().getAddress1();
		sBillAddr2 = order.getBillingInfo().getBillingContact().getAddress().getAddress2();
		sBillCity = order.getBillingInfo().getBillingContact().getAddress().getCityOrTown();
		sBillState = order.getBillingInfo().getBillingContact().getAddress().getStateOrProvince();
		sBillZip = order.getBillingInfo().getBillingContact().getAddress().getPostalOrZipCode();
		sBillCntry = order.getBillingInfo().getBillingContact().getAddress().getCountryCode();
		sBillHome = order.getBillingInfo().getBillingContact().getPhoneNumbers().getHome();
		sBillMobile = order.getBillingInfo().getBillingContact().getPhoneNumbers().getMobile();
		sBillWork = order.getBillingInfo().getBillingContact().getPhoneNumbers().getWork();
		
		sShipComp = " ";
		sShipFName = " ";
		sShipLName = " ";
		sShipAddr1 = " ";
		sShipAddr2 = " ";
		sShipCity = " ";
		sShipState = " ";
		sShipZip = " ";
		sShipCntry = " ";
		sShipHome = " ";
		sShipMobile = " ";
		sShipWork = " ";
		if(order.getFulfillmentInfo() != null 
			&& order.getFulfillmentInfo().getFulfillmentContact() != null
		    && order.getFulfillmentInfo().getFulfillmentContact().getCompanyOrOrganization() != null)
		{
			sShipComp = order.getFulfillmentInfo().getFulfillmentContact().getCompanyOrOrganization();
			sShipFName = order.getFulfillmentInfo().getFulfillmentContact().getFirstName();
			sShipLName = order.getFulfillmentInfo().getFulfillmentContact().getLastNameOrSurname();
			sShipAddr1 = order.getFulfillmentInfo().getFulfillmentContact().getAddress().getAddress1();
			sShipAddr2 = order.getFulfillmentInfo().getFulfillmentContact().getAddress().getAddress2();
			sShipCity = order.getFulfillmentInfo().getFulfillmentContact().getAddress().getCityOrTown();
			sShipState = order.getFulfillmentInfo().getFulfillmentContact().getAddress().getStateOrProvince();
			sShipZip = order.getFulfillmentInfo().getFulfillmentContact().getAddress().getPostalOrZipCode();
			sShipCntry = order.getFulfillmentInfo().getFulfillmentContact().getAddress().getCountryCode();
			sShipHome = order.getFulfillmentInfo().getFulfillmentContact().getPhoneNumbers().getHome();
			sShipMobile = order.getFulfillmentInfo().getFulfillmentContact().getPhoneNumbers().getMobile();
			sShipWork = order.getFulfillmentInfo().getFulfillmentContact().getPhoneNumbers().getWork();
		}
		
		sShipMethod = order.getFulfillmentInfo().getShippingMethodCode();
		sShipCost = BigDecimal.valueOf(order.getShippingTotal()).toString();
		
		sCreatedDate = order.getAuditInfo().getCreateDate().toString();
		
		sIpAddr = order.getIpAddress();			
		sLastModDt = order.getAuditInfo().getUpdateDate().toString(); 
		sOrdSts = order.getStatus();
		sLastModBy = order.getAuditInfo().getUpdateBy();
		sShipRes = "N"; 
		if (order.getFulfillmentInfo().getIsDestinationCommercial()){ sShipRes = "Y";  };
		
		// tax rate 1,2,3 tax amt 1,2,3 ?????
		sTaxTot = BigDecimal.valueOf(order.getTaxTotal()).toString(); // ???? not rate
		sSubtot = BigDecimal.valueOf(order.getSubtotal()).toEngineeringString();
		sTotal = BigDecimal.valueOf(order.getTotal()).toEngineeringString();
		sTotalPaid = BigDecimal.valueOf(order.getTotalCollected()).toEngineeringString();
		
		// save header 
		
		wrtord.saveOrderHdr(sSite, sOrdNum, sCustNum
		 , sBillComp, sBillFName, sBillLName, sBillAddr1, sBillAddr2, sBillCity, sBillState, sBillZip
		 , sBillCntry, sBillHome, sBillMobile, sBillWork
		 , sShipComp, sShipFName, sShipLName, sShipAddr1, sShipAddr2, sShipCity, sShipState, sShipZip
		 , sShipCntry, sShipHome, sShipMobile, sShipWork, sShipMethod, sShipCost
		 , sCreatedDate, sIpAddr, sLastModDt, sLastModBy, sOrdSts, sShipRes
		 , sTaxTot, sSubtot, sTotal, sTotalPaid);
				 
		getPayments(order);
		
		getShipments(order);
	}
	//===================================================================
	// save Payment Log
	//===================================================================
	private void getPayments(Order order) throws Exception
	{
		List<Payment> lPays = order.getPayments();
		Vector<String> vPays = new Vector<String>();
		Vector<String> vPayType = new Vector<String>();
		Vector<String> vPayCardType = new Vector<String>();
		for(Payment pays : lPays)
		{
			vPays.add(BigDecimal.valueOf(pays.getAmountCollected()).toString());			    
			String sPayType = pays.getPaymentType();
			vPayType.add(sPayType);
	    
			String sPayCardType = " ";
			if(!sPayType.equals("StoreCredit"))
			{
				sPayCardType = pays.getBillingInfo().getCard().getPaymentOrCardType();
			}
			vPayCardType.add(sPayCardType);
		}
	
		// 	save payments
		wrtord.savePayment(sSite, sOrdNum, vPays, vPayType, vPayCardType);
	}
	//===================================================================
	// save items
	//===================================================================
	private void getShipments(Order order)
	{
		List<Shipment> ships = order.getShipments();
		Vector<String> vShipMethod = new Vector<String>();
		Vector<String> vShipCost = new Vector<String>();
		for(Shipment ship : ships)
		{
			vShipMethod.add(ship.getShippingMethodCode());
			vShipCost.add(BigDecimal.valueOf(ship.getCost()).toString());				
		}
		// save Shipments
		wrtord.saveShipments(sSite, sOrdNum, vShipMethod, vShipCost);
	}
	//===================================================================
	// get items
	//===================================================================
	private void getItems(Order order) throws Exception
	{
		List<OrderItem> ordItem = order.getItems();
		
		for(OrderItem item : ordItem)
		{					
			sItmProd = item.getProduct().getVariationProductCode();
			sItmQty = Integer.toString(item.getQuantity());
			sItmPrc = item.getProduct().getPrice().toString();
			sItmRet = item.getUnitPrice().toString();
			sItmExtRet = BigDecimal.valueOf(item.getExtendedTotal()).toString();
			sItmDisc = BigDecimal.valueOf(item.getDiscountTotal()).toString();
			sItmTotal = BigDecimal.valueOf(item.getTotal()).toString();
			
			wrtord.saveOrderDtl(sSite, sOrdNum, sItmProd, sItmQty, sItmPrc, sItmRet, sItmExtRet
					, sItmDisc, sItmTotal);
			
			if(item.getFulfillmentLocationCode().equals("Pickup"))
			{
				vItmPickLoc.add(item.getFulfillmentLocationCode());
			} 
			else{vItmPickLoc.add("NONE");}
			
			List<AppliedLineItemProductDiscount> prodDiscs = item.getProductDiscounts();				
			Vector<String> vItmDiscOrder = new Vector<String>();
			Vector<String> vItmDiscProd = new Vector<String>();
			Vector<String> vItmDiscCode = new Vector<String>();
			Vector<String> vItmDiscQty = new Vector<String>();
			Vector<String> vItmDiscAmt = new Vector<String>();
			boolean bDiscExists = false; 
			for(AppliedLineItemProductDiscount alipDisc : prodDiscs)
			{
				vItmDiscOrder.add(sOrdNum);
				vItmDiscProd.add(sItmProd);					
				vItmDiscCode.add(alipDisc.getCouponCode());
				vItmDiscQty.add(Integer.toString(alipDisc.getDiscountQuantity()));
				vItmDiscAmt.add(BigDecimal.valueOf(alipDisc.getImpact()).toString());
				bDiscExists = true;
			}			
			
			// save item discount details
			if(bDiscExists)
			{
				wrtord.saveItemDisc(sSite, sOrdNum, sItmProd, vItmDiscCode, vItmDiscQty, vItmDiscAmt);
			}
		}
	}
		
	/**
	 * ===================================================================
	 * test this class
	 * ===================================================================
	 */
	public static void main(String[] args) 
	{
		RtvMozuOrders_Srv morder = new RtvMozuOrders_Srv();
	}
}
