package com.test.api;


import java.io.*;
import java.math.*;
import java.util.*;

import rciutility.CallAs400SrvPgmSup;

import com.mozu.api.contracts.productadmin.Attribute;
import com.mozu.api.contracts.productadmin.AttributeCollection;
import com.mozu.api.contracts.productadmin.AttributeInProductType;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValue;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueInProductType;
import com.mozu.api.contracts.productadmin.ProductType;
import com.mozu.api.contracts.productadmin.ProductTypeCollection;
import com.mozu.api.contracts.productadmin.ProductVariation;
import com.mozu.api.resources.commerce.catalog.admin.attributedefinition.*;
import com.mozu.api.ApiContext;


public class RtvBaseAttr   extends CallAs400SrvPgmSup
{
	private ApiContext apiContext;
	private AttributeResource attres = null;
	ProductTypeResource prodType = null;
	
	private String sTypeId = null; 
	private String sType = null;
	private String sAttr = null;
	private String sOption = null;
	private String sOptId = null;
	
	public RtvBaseAttr(String attr) 
	{
		this.sAttr = attr;
		try {
			setSrvPgmName("RCI", "MOATTRSV");
		
			MozuInit mi = new MozuInit();
			apiContext = mi.getApiCont();
		
			attres = new AttributeResource(apiContext);
			
			prodType = new ProductTypeResource(apiContext);
			ProductTypeCollection ptcBase = prodType.getProductTypes(0, 0, null, "name sw 'base'", null);
			List <ProductType> lptBase = ptcBase.getItems();
			ProductType ptBase = lptBase.get(0);
			sType = ptBase.getName();
			sTypeId = Integer.toString(ptBase.getId());
			
			Attribute attrib = attres.getAttribute(sAttr);
			sAttr = attrib.getAttributeFQN();
			
			List<AttributeVocabularyValue> lavvPtVal = attrib.getVocabularyValues();
			for(AttributeVocabularyValue avvPtVal : lavvPtVal)
			{
				sOption = avvPtVal.getValue().toString();
				saveAttr();
			}			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
	/**
	 * ===================================================================
	 * save in ibm i 
	 * =================================================================== */
	private void saveAttr()
    {
    	StringBuffer sbInp = new StringBuffer();
        sbInp.append(setParamString(sTypeId, 5));
        sbInp.append(setParamString(sAttr, 50));
        sbInp.append(setParamString(sOption, 50));

        String [] sParam = new String[]{sbInp.toString()};
        int [] iPrmLng = { sbInp.length() };
        runSrvPgmProc("SAVEATTR", sParam, iPrmLng);
    }
	/**
	 * ===================================================================
	 * test 
	 * =================================================================== */
	public static void main(String[] args) 
	{
		RtvBaseAttr rtvbase = new RtvBaseAttr("tenant~normalizedcolor");		
	}

}
