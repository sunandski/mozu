package com.test.api;

import java.util.Date;
import java.util.Vector;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.test.api.*;

import rciutility.RunSQLStmt;

public class ReplicateVolColors1 extends RunSQLStmt
{
    Vector<String> vType = new Vector();
    Vector<String> vColor = new Vector();
    
	public ReplicateVolColors1() 
	{
		try 
		{
			CreateAttribute crtattr = new CreateAttribute();
			setTypeColorList();
		
			//for(int i=0; i < vType.size(); i++)
			for(int i=0; i < 1; i++)	
			{	
				System.out.println("\n"+ i + ". Type=" + vType.get(i));
				//for(int j=0; j < vColor.size(); j++)
				for(int j=5900; j < 6200; j++)
				{
					if(j%100==0){System.out.println("   | "+ j + ". Color=" + vColor.get(j) );}
					
					crtattr.setNewAttrinProdType("Tenant~Color", vType.get(i), vColor.get(j)); 
					if(crtattr.isInError())
					{
						System.out.println(j + ". Color=" + vColor.get(j) + " Error ==> " + crtattr.getError());
					}
				}
			}
		
		
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}

	//=====================================================================
	// set type and color array
	//=====================================================================
	private void setTypeColorList() throws SQLException
	{
		String sStmt = " select pttype, ptid from Rci.MoPrdTy"
		           + " group by pttype, ptid"
		           + " order by pttype"		            		         
		        ;
				System.out.println(sStmt);
				setPrepStmt(sStmt);
				ResultSet rs = runQuery();
				while(readNextRecord())
				{
					vType.add(getData("PtType").trim());
				}
				rs.close();	
				
				sStmt = " select vcclr from Rci.volclr order by vcclr";				
				setPrepStmt(sStmt);
				rs = runQuery();
				while(readNextRecord())
				{
					vColor.add(getData("vcclr").trim());
				}
				rs.close();				
				disconnect();
	}
	
	// run
	public static void main(String[] args) 
	{
		ReplicateVolColors1 rplvolclr = new ReplicateVolColors1();
	}

}
