package com.test.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.io.*;
import java.math.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mozu.api.contracts.commerceruntime.discounts.AppliedLineItemProductDiscount;
import com.mozu.api.contracts.commerceruntime.fulfillment.Shipment;
import com.mozu.api.contracts.commerceruntime.payments.Payment;
import com.mozu.api.contracts.location.Location;
import com.mozu.api.contracts.location.LocationCollection;
import com.mozu.api.contracts.productadmin.ProductOption;
import com.mozu.api.contracts.content.Document;
import com.mozu.api.contracts.productadmin.LocationInventory;
import com.mozu.api.contracts.productadmin.LocationInventoryAdjustment;
import com.mozu.api.contracts.productadmin.LocationInventoryCollection;
import com.mozu.api.contracts.productadmin.Product;
import com.mozu.api.contracts.productadmin.ProductCategory;
import com.mozu.api.contracts.productadmin.ProductCollection;
import com.mozu.api.contracts.productadmin.ProductInCatalogInfo;
import com.mozu.api.contracts.productadmin.ProductInventoryInfo;
import com.mozu.api.contracts.productadmin.ProductLocalizedImage;
import com.mozu.api.contracts.productadmin.ProductOptionValue;
import com.mozu.api.contracts.productadmin.ProductPrice;
import com.mozu.api.contracts.productadmin.ProductProperty;
import com.mozu.api.contracts.productadmin.ProductPropertyValue;
import com.mozu.api.contracts.productadmin.ProductPublishingInfo;
import com.mozu.api.contracts.productadmin.ProductVariation;
import com.mozu.api.contracts.productadmin.ProductVariationDeltaPrice;
import com.mozu.api.contracts.productadmin.ProductVariationPagedCollection;
import com.mozu.api.contracts.productruntime.ProductImage;
import com.mozu.api.resources.commerce.admin.LocationResource;
import com.mozu.api.resources.commerce.catalog.admin.ProductResource;
import com.mozu.api.resources.content.documentlists.DocumentResource;
import com.mozu.api.ApiContext;
import com.mozu.api.MozuApiContext;
import com.mozu.api.resources.commerce.catalog.admin.products.LocationInventoryResource;
import com.mozu.api.resources.commerce.catalog.admin.products.ProductVariationResource;



public class MozuProductsByFiltr extends MozuInit 
{

	private String sSite = "2659";// ???? how to get it from properties file
	String sProd = " ";
	String sBaseProd = " ";
	String sName = "Y";
	String sFullDesc = "Y";
	String sShortDesc = "Y";   
	String sProdImg = " ";
	String sCateg = "Y";	
	String sMsrp = "0";
	String sMap = "0";
	String sPrice = "0";
	String sSlsPrice = "0";	
	String sWeight = "0";
	String sOnHand = "0";
	Vector vOptNm = new Vector();
	Vector vOptLst = new Vector();
	Vector vChild = new Vector();
	Vector vChildLoc = new Vector();
	Vector vChildInv = new Vector();
	
	private ApiContext apiContext;
	ProductResource productResource = null;
	ProductVariationResource pvrChild = null;
	DocumentResource docresImage = null;
	LocationResource locresInventory = null;
	LocationInventoryResource locinvresInventory = null;
    
	
	private String [] sLoc = null;
	
	/**
	 * ===================================================================
	 * Constructor - initialize connection via super class
	 * =================================================================== */
	public MozuProductsByFiltr() 
	{
		try 
		{  
			System.out.println("MozuProducts init();");
			init();
			apiContext = super.getApiCont();
			productResource = new ProductResource(apiContext);
			pvrChild = new ProductVariationResource(apiContext);
			docresImage = new DocumentResource(apiContext);
			locresInventory = new LocationResource(apiContext);
			locinvresInventory = new LocationInventoryResource(apiContext);
            
			
			SASS_refresh_locations sassloc = new SASS_refresh_locations(apiContext);
			sassloc.setSASSLocation();
			sLoc = sassloc.getSASSLocation();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}
	
	
	/**
	 * ===================================================================
	 * get product
	 * =================================================================== */
	public void getProductsByFilter(String sFilter) throws Exception 
	{
		Integer maxCountPerPage = 50;
		
		ProductCollection productCollection = productResource
				.getProducts(null, null, null, sFilter, null, null, null, null);
		Integer total = productCollection.getTotalCount();
		if (total.intValue() > 0) {
            int totalPages = (int) Math.ceil(total.doubleValue() / maxCountPerPage.doubleValue());
            int iCount = 1;
            for (int i = 0; i < totalPages; i++) {
                int startIndex = i * maxCountPerPage;
                ProductCollection items = productResource.getProducts(startIndex, maxCountPerPage, null, sFilter, null, null,
                        null, null);
                for (Product prod : items.getItems()) 
                {
                	getSingleProduct(prod, iCount++);
                	
                }
            }
        }	
	}
	
	
	//===================================================================
	// save single product
	// ===================================================================	 
	private void getSingleProduct(Product prod, int iCount)
	{
		try{				
			sSite = "2659";// ???? how to get it from properties file
			sProd = prod.getProductCode();
			sBaseProd = prod.getBaseProductCode();
			System.out.println(iCount + ".  BaseProd=" + sBaseProd + " sProd=" + sProd);
			sName = "Y";
			if(prod.getContent().getProductName() == null){ sName = "N"; }
			sFullDesc = "Y";
			if(prod.getContent().getProductFullDescription() == null){ sFullDesc = "N"; }
			sShortDesc = "Y";				
			if(prod.getContent().getProductShortDescription() == null){sShortDesc = "N";}
			
			// get product image properties
			sProdImg = getImgAvail(prod);
			
			sCateg = "Y";				
			if(prod.getProductInCatalogs() == null ) {  sCateg = "N"; }				
			
			sMsrp = "0";
			if(prod.getPrice().getMsrp() != null)
			{					
				sMsrp = BigDecimal.valueOf(prod.getPrice().getMsrp()).toString();
			}
			
			sMap = "0";
			if(prod.getPrice().getMap() != null)
			{
				sMap = BigDecimal.valueOf(prod.getPrice().getMap()).toString();
			}
			
			sPrice = "0";
			if(prod.getPrice().getPrice()  != null)
			{
				sPrice = BigDecimal.valueOf(prod.getPrice().getPrice()).toString();
			}
			sSlsPrice = "0";
			if(prod.getPrice().getSalePrice() != null){
				sSlsPrice = BigDecimal.valueOf(prod.getPrice().getSalePrice()).toString();
			}			
			
			// get product Child/variations
			getProdChild();
			
			// get product options
			getProdOpt(prod);						
			
			// get all properties - now get weight only
			getProdProp(prod);			
		}
    	catch (Exception e) { e.printStackTrace(); }
	}
	//===================================================================
	// get product Child/variations
	//===================================================================
	private void getProdChild()
	{		
		try {
			
			ProductVariationPagedCollection product;
		
			product = pvrChild.getProductVariations(sProd);
			for (ProductVariation pv : product.getItems()) 
			{				
				System.out.println(pv.getVariationProductCode());
				getInventory(pv.getVariationProductCode());
				getPrice(pv, sProd);
			}
		} 
		catch (Exception e) { e.printStackTrace();}
	}
	//===================================================================
	// get product options
	// ===================================================================	 
	private void getProdOpt(Product prod)  throws Exception
	{
		List<ProductOption> lOptions = prod.getOptions();
		if(lOptions != null)
		{
			for(ProductOption option : lOptions)
			{
				vOptNm.add(option.getAttributeFQN());
				List<ProductOptionValue> lOptVal = option.getValues();
				Vector vVal  = new Vector();
				
				if(lOptVal != null)
				{						 
					for(ProductOptionValue optval : lOptVal)
					{
						vVal.add(optval.getValue());
						System.out.println(option.getAttributeFQN() + " = " + optval.getValue());
					}
				}
				vOptLst.add(vVal);
				
			}
		}
	}
	//===================================================================
	// get product properties
	// ===================================================================	 
	private void getProdProp(Product prod) throws Exception 
	{
		List<ProductProperty> lProps = prod.getProperties();
		if(lProps != null)
		{					
			for(ProductProperty prop : lProps)
			{
				String sKey = prop.getAttributeFQN();
				if(sKey != null)
				{
					List<ProductPropertyValue> lPpValues = prop.getValues();
					if(lPpValues != null)
					{
						for(ProductPropertyValue lppval : lPpValues)
						{
							String sVal = lppval.getValue().toString();									
							if(sKey.indexOf("Tenant~Weight") >= 0){	sWeight = sVal; }
							//System.out.println(sKey + " = " + sVal);
						}	
					}
				}
			}
		}
	}
	//===================================================================
	// check if image added to product setup 
	// ===================================================================	 
	private String getImgAvail(Product prod)
	{
		String sImg = "N";
		String sImgDocNm = "NONE";
		List<ProductLocalizedImage> lProdImg = prod.getContent().getProductImages();
		if(lProdImg != null)
		{
			for(ProductLocalizedImage img : lProdImg)
			{
				String ImgId = img.getCmsId();
				if(ImgId != null )
				{ 
					sImg = "Y"; 
				    sImgDocNm = getDocument(ImgId);
				}				 
			}
		}	

		return sImg;
	}
	
	//===================================================================
	// get document properties 
	// //03/10/2015 - run with error Not authorized to perform 'DocumentList/GetDocument'
	// // need Joanne help  
	// ===================================================================	 
	private String getDocument(String cmsId) {
		String sImage = "NONE";
        try {
            
            Document doc = docresImage.getDocument("files@mozu", cmsId);
            sImage = doc.getName();
            
            // for test only
            //System.out.println(" docProp=" + doc.getListFQN() + doc.getName());
            //ObjectMapper mapper = new ObjectMapper();
            //System.out.println(mapper.writerWithDefaultPrettyPrinter()
            //.writeValueAsString(doc));
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return sImage;
    }

	
	
	//===================================================================
	// get product inventory
	//===================================================================
    private void getInventory(String code) throws Exception 
    {
        
        LocationCollection lcollection = locresInventory.getLocations();
        List<Location> llist = lcollection.getItems();
        for (Location location : llist) {
            try {  
            	for(String sloc : sLoc)
                {
                	try {
                	if(location.getCode().equals(sloc))
                	{ 
                	LocationInventory inventory = locinvresInventory.getLocationInventory(code, sloc);
                   
                	vChild.add(code);                  
                	vChildLoc.add(sloc);
                	String sqty = inventory.getStockOnHand().toString(); 
                	Integer iqty = inventory.getStockOnHand();
                	
                	vChildInv.add(sqty);
                	System.out.println(code + " - on hand: " + sloc + " " + sqty);
                	iqty = new Integer( iqty.intValue() + 5);
                	inventory.setStockOnHand(iqty);
                	
                	LocationInventoryAdjustment liaWhs = new LocationInventoryAdjustment();
                	liaWhs.setLocationCode(location.getCode());
                	liaWhs.setProductCode(code);
                	liaWhs.setValue(iqty);
                	liaWhs.setType("ABSOLUTE");
                	List<LocationInventoryAdjustment> lliaWhs = new ArrayList<LocationInventoryAdjustment>();
                	lliaWhs.add(liaWhs);
                	locinvresInventory.updateLocationInventory(lliaWhs, code);
                	}
                	} catch (Exception e) { e.printStackTrace(); }
                }
                
            } catch (Exception e) {            	
            	//System.out.println(e.getMessage());
            	e.printStackTrace();
            }
            
           
        }
    }	
  //===================================================================
  	// get product inventory
  	//===================================================================
      private void getPrice(ProductVariation product, String sBaseCode)
      {
    	  try {
    	  ProductVariationDeltaPrice pp = product.getDeltaPrice();
		  pp.setValue(5.99);
		  
		  String sKey = product.getVariationkey();
		  product.setDeltaPrice(pp);
		  pvrChild.updateProductVariation(product,sBaseCode, sKey); 
    	  } 
    	  catch (Exception e) 
    	  {
    		  e.printStackTrace();
    	  }
      }
	/**
	 * ===================================================================
	 * test
	 * ===================================================================	 */
	public static void main(String[] args) 
	{
		MozuProductsByFiltr mprod = new MozuProductsByFiltr();
		try {
			//mprod.getProducts();
			String sFilter = "ProductCode eq 2203051473310";
			System.out.println("sssss");
			mprod.getProductsByFilter(sFilter);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
