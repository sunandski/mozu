package com.test.api;

import java.io.*;
import java.math.*;
import java.util.*;

import rciutility.CallAs400SrvPgmSup;

import com.mozu.api.contracts.productadmin.AttributeInProductType;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueInProductType;
import com.mozu.api.contracts.productadmin.ProductType;
import com.mozu.api.contracts.productadmin.ProductTypeCollection;
import com.mozu.api.contracts.productadmin.ProductVariation;
import com.mozu.api.resources.commerce.catalog.admin.attributedefinition.*;
import com.mozu.api.ApiContext;


public class RtvAttributeLst  extends CallAs400SrvPgmSup
{
	private ApiContext apiContext;
	private AttributeResource attres = null;
	
	private String sTypeId = null; 
	private String sType = null;
	private String sAttr = null;
	private String sOption = null;
	private String sOptId = null;
	
	/**
	 * ===================================================================
	 * retreive attribute list and save on ibm i 
	 * =================================================================== */
	public RtvAttributeLst() 
	{
		try {
			setSrvPgmName("RCI", "MOATTRSV");
			
			 
			MozuInit mi = new MozuInit();
			apiContext = mi.getApiCont();
						
			ProductTypeResource prodType = new ProductTypeResource(apiContext);
			ProductTypeCollection prodcol = prodType.getProductTypes();
			List<ProductType>  lprodty = prodcol.getItems();
			for(ProductType ptype : lprodty)
			{			    
			    AttributeInProductType aptOpt = new AttributeInProductType();
			    sTypeId = Integer.toString(ptype.getId());
			    sType = ptype.getName();
			    
			    List<AttributeInProductType> lOpt =  ptype.getOptions();
				for(AttributeInProductType opt : lOpt)
				{
					sAttr = opt.getAttributeFQN();
					
					List<AttributeVocabularyValueInProductType> lvocVal = opt.getVocabularyValues();
					for(AttributeVocabularyValueInProductType v : lvocVal)
					{
						sOption = v.getValue().toString();
						saveAttr();
					}
				}
			}
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
    private void saveAttr()
    {
    	StringBuffer sbInp = new StringBuffer();
        sbInp.append(setParamString(sTypeId, 5));
        sbInp.append(setParamString(sAttr, 50));
        sbInp.append(setParamString(sOption, 50));

        String [] sParam = new String[]{sbInp.toString()};
        int [] iPrmLng = { sbInp.length() };
        runSrvPgmProc("SAVEATTR", sParam, iPrmLng);
    }
	/**
	 * ===================================================================
	 * test 
	 * =================================================================== */
	public static void main(String[] args) 
	{
		RtvAttributeLst rtvattr = new RtvAttributeLst();
	}

}
