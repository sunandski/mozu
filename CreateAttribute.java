/**
 * 
 */
package com.test.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.io.*;
import java.math.*;

import com.mozu.api.contracts.productadmin.Attribute;
import com.mozu.api.contracts.productadmin.AttributeInProductType;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValue;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueInProductType;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueLocalizedContent;
import com.mozu.api.contracts.productadmin.ProductType;
import com.mozu.api.contracts.productadmin.ProductTypeCollection;
import com.mozu.api.resources.commerce.catalog.admin.attributedefinition.*;
import com.mozu.api.ApiContext;

/**
 * @author vrozen
 * @ttile Create new attribute in Mozu 
 */
public class CreateAttribute  extends MozuInit
{
	private ApiContext apiContext = null;
	private AttributeResource attres = null;	
	private Attribute attrib = null;
	private ProductTypeResource prodType = null;
	private ProductTypeCollection prodcol = null;
	private AttributeInProductType aptOpt = null;
	private List<AttributeVocabularyValue> lvocValLoc = null;
	private AttributeVocabularyValueInProductType vocVal = null;
	private boolean bError = false;
	private String sError = "NONE";
	
	/** =======================================================
	 * Constructor - connect to mozu
	 * ======================================================= */
	public CreateAttribute() 
	{
		try {
				apiContext = super.getApiCont();
				attres = new AttributeResource(apiContext);
				prodType = new ProductTypeResource(apiContext);
			} 
		    catch (Exception e)
		    {
		    	System.out.println("\n" + e.getMessage());
		    	bError = true;
		    	sError = e.getMessage();
		    }
	}
	
	//=======================================================
	// s
	//=======================================================
    public void setNewAttrinProdType(String sAttr, String sProdType, String sValue)
    {
    	try {
    		
    		setNewAttr( sAttr, sValue);			 
			
			prodcol = prodType.getProductTypes(0,1,null, "Name eq " + sProdType , null);
	
			List<ProductType>  lprodty = prodcol.getItems();
			for(ProductType ptype : lprodty)
			{					
				List<AttributeInProductType> lopProdTyp = ptype.getOptions();
				int i = 0;
				for(AttributeInProductType opProdTyp : lopProdTyp)
				{
					if(opProdTyp.getAttributeFQN().toLowerCase().equals(sAttr.toLowerCase()))
					{
						List<AttributeVocabularyValueInProductType> lavvptValue = opProdTyp.getVocabularyValues();
						lavvptValue.add(vocVal);
						opProdTyp.setVocabularyValues(lavvptValue);
						lopProdTyp.set(i, opProdTyp);
						break;
					}
					i++;
				}
				
				
				ptype.setOptions(lopProdTyp);
				prodType.updateProductType(ptype, ptype.getId());				
			}
		} 
	    catch (Exception e) 
	    {
	    	System.out.println("\n" + e.getMessage());
	    	bError = true;
	    	sError = e.getMessage(); 
	    }
    }
    //=======================================================
  	// set new attribute
  	//=======================================================
    public void setNewAttr(String sAttr, String sValue)
    {
    	// set new attribute
    	try {
    	
    		attrib = attres.getAttribute(sAttr);
    		aptOpt = new AttributeInProductType();
    		aptOpt.setAttributeFQN(attrib.getAttributeFQN());
    		aptOpt.setIsInheritedFromBaseType(false);
    		aptOpt.setAttributeDetail(attrib);
    		aptOpt.setIsHiddenProperty(false);
    		aptOpt.setIsMultiValueProperty(false);
    		aptOpt.setIsRequiredByAdmin(false);
    		aptOpt.setOrder(0);			
			
    		AttributeVocabularyValueLocalizedContent vocValLocolized = new AttributeVocabularyValueLocalizedContent();				
    		vocValLocolized.setStringValue(sValue);
    		vocValLocolized.setLocaleCode("en-US");
		
    		AttributeVocabularyValue vocValLoc = new AttributeVocabularyValue();
    		vocValLoc.setValue(sValue);
    		vocValLoc.setValueSequence(2736);
    		vocValLoc.setContent(vocValLocolized); 
        
    		vocVal = new AttributeVocabularyValueInProductType();
    		vocVal.setValue(sValue);
    		vocVal.setOrder(0);
    		vocVal.setVocabularyValueDetail(vocValLoc);
		
    		List<AttributeVocabularyValueInProductType> lvocval  =  new ArrayList<AttributeVocabularyValueInProductType>();
    		lvocval.add(vocVal);
		
    		aptOpt.setVocabularyValues(lvocval);
		
    		lvocValLoc = attrib.getVocabularyValues();
    		lvocValLoc.add(vocValLoc);
    		attrib.setVocabularyValues(lvocValLoc);
		
    		attres.updateAttribute(attrib, sAttr);
    	} catch (Exception e) 
    	{
    		//System.out.println("\n" + e.getMessage());
	    	bError = true;
	    	sError = e.getMessage();
		}	
    }
    
    //=======================================================
    // return error status 
    //=======================================================
    public boolean isInError(){return bError; }
    public String getError(){return sError; }
    
	/** =======================================================
	 * test
	 * ======================================================= */
	public static void main(String[] args) 
	{
		CreateAttribute crtattr = new CreateAttribute();
		//crtattr.setNewAttr("Tenant~Color", "Red_001");
		crtattr.setNewAttrinProdType("Tenant~Color", "Apparel", "Red_001");
	}

}
