package com.test.api;
/*
 * Copy volusion colors in Mozu attributes
 */
import java.util.Date;
import java.util.Vector;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.test.api.*;

import rciutility.RunSQLStmt;

public class ReplicateVolClrInAttr extends RunSQLStmt
{
    Vector<String> vId = new Vector();
    Vector<String> vColor = new Vector();
    
	public ReplicateVolClrInAttr() 
	{
		try 
		{			
			setColorList();
		
			CreateAttribute1 crtattr = new CreateAttribute1();
			String sAttr = "Tenant~Color";
			crtattr.rtvAttr(sAttr);
		
			for(int i=0; i < vColor.size(); i++)			
			{
				System.out.println(i + ". Color=" + vColor.get(i) );
					
				crtattr.setNewAttr(sAttr, vId.get(i)  , vColor.get(i)); 
				if(crtattr.isInError())
				{
					System.out.println(i + ". Color=" + vColor.get(i) 
						+ " Error ==> " + crtattr.getError());
				}
				
			}
			
			// update all colors in Mozu together
			crtattr.updAttr(sAttr); 		
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}

	//=====================================================================
	// set type and color array
	//=====================================================================
	private void setColorList() throws SQLException
	{
		String sStmt = " select vcid, vcclr from Rci.volclr order by vcclr";				
		setPrepStmt(sStmt);
		ResultSet rs = runQuery();
		while(readNextRecord())
		{
			vId.add(getData("vcid").trim());
			vColor.add(getData("vcclr").trim());
		}
		rs.close();				
		disconnect();
	}
	
	// run
	public static void main(String[] args) 
	{
		ReplicateVolClrInAttr rplvolclr = new ReplicateVolClrInAttr();
	}

}
