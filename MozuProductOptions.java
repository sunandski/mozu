package com.test.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.io.*;
import java.math.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mozu.api.contracts.commerceruntime.discounts.AppliedLineItemProductDiscount;
import com.mozu.api.contracts.commerceruntime.fulfillment.Shipment;
import com.mozu.api.contracts.commerceruntime.payments.Payment;
import com.mozu.api.contracts.location.Location;
import com.mozu.api.contracts.location.LocationCollection;
import com.mozu.api.contracts.productadmin.ProductOption;
import com.mozu.api.contracts.content.Document;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValue;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueLocalizedContent;
import com.mozu.api.contracts.productadmin.LocationInventory;
import com.mozu.api.contracts.productadmin.LocationInventoryAdjustment;
import com.mozu.api.contracts.productadmin.LocationInventoryCollection;
import com.mozu.api.contracts.productadmin.Product;
import com.mozu.api.contracts.productadmin.ProductCategory;
import com.mozu.api.contracts.productadmin.ProductCollection;
import com.mozu.api.contracts.productadmin.ProductInCatalogInfo;
import com.mozu.api.contracts.productadmin.ProductInventoryInfo;
import com.mozu.api.contracts.productadmin.ProductLocalizedImage;
import com.mozu.api.contracts.productadmin.ProductOptionValue;
import com.mozu.api.contracts.productadmin.ProductPrice;
import com.mozu.api.contracts.productadmin.ProductProperty;
import com.mozu.api.contracts.productadmin.ProductPropertyValue;
import com.mozu.api.contracts.productadmin.ProductPropertyValueLocalizedContent;
import com.mozu.api.contracts.productadmin.ProductPublishingInfo;
import com.mozu.api.contracts.productadmin.ProductVariation;
import com.mozu.api.contracts.productadmin.ProductVariationCollection;
import com.mozu.api.contracts.productadmin.ProductVariationDeltaPrice;
import com.mozu.api.contracts.productadmin.ProductVariationOption;
import com.mozu.api.contracts.productadmin.ProductVariationPagedCollection;
import com.mozu.api.contracts.productruntime.ProductImage;
import com.mozu.api.resources.commerce.admin.LocationResource;
import com.mozu.api.resources.commerce.catalog.admin.ProductResource;
import com.mozu.api.resources.content.documentlists.DocumentResource;
import com.mozu.api.ApiContext;
import com.mozu.api.MozuApiContext;
import com.mozu.api.resources.commerce.catalog.admin.products.LocationInventoryResource;
import com.mozu.api.resources.commerce.catalog.admin.products.ProductVariationResource;
import com.mozu.api.resources.commerce.catalog.admin.products.ProductOptionResource;


public class MozuProductOptions extends MozuInit 
{

	private String sSite = "2659";// ???? how to get it from properties file
	String sProd = " ";
	String sBaseProd = " ";
	String sName = "Y";
	String sFullDesc = "Y";
	String sShortDesc = "Y";   
	String sProdImg = " ";
	String sCateg = "Y";	
	String sMsrp = "0";
	String sMap = "0";
	String sPrice = "0";
	String sSlsPrice = "0";	
	String sWeight = "0";
	String sOnHand = "0";
	Vector vOptNm = new Vector();
	Vector vOptLst = new Vector();
	Vector vChild = new Vector();
	Vector vChildLoc = new Vector();
	Vector vChildInv = new Vector();
	
	private ApiContext apiContext;
	ProductResource productResource = null;
	ProductVariationResource pvrChild = null;
	DocumentResource docresImage = null;
	LocationResource locresInventory = null;
	LocationInventoryResource locinvresInventory = null;
	ProductOptionResource porOpt = null;
    
	
	private String [] sLoc = null;
	
	/**
	 * ===================================================================
	 * Constructor - initialize connection via super class
	 * =================================================================== */
	public MozuProductOptions() 
	{
		try 
		{  
			System.out.println("MozuProducts init();");
			init();
			apiContext = super.getApiCont();
			productResource = new ProductResource(apiContext);
			pvrChild = new ProductVariationResource(apiContext);
			docresImage = new DocumentResource(apiContext);
			locresInventory = new LocationResource(apiContext);
			locinvresInventory = new LocationInventoryResource(apiContext);
			porOpt = new ProductOptionResource(apiContext);
			
			SASS_refresh_locations sassloc = new SASS_refresh_locations(apiContext);
			sassloc.setSASSLocation();
			sLoc = sassloc.getSASSLocation();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}
	
	
	/**
	 * ===================================================================
	 * get product
	 * =================================================================== */
	public void getProductsByFilter(String sFilter) throws Exception 
	{
		Integer maxCountPerPage = 50;
		
		ProductCollection productCollection = productResource
				.getProducts(null, null, null, sFilter, null, null, null, null);
		Integer total = productCollection.getTotalCount();
		if (total.intValue() > 0) {
            int totalPages = (int) Math.ceil(total.doubleValue() / maxCountPerPage.doubleValue());
            int iCount = 1;
            for (int i = 0; i < totalPages; i++) {
                int startIndex = i * maxCountPerPage;
                ProductCollection items = productResource.getProducts(startIndex, maxCountPerPage, null, sFilter, null, null,
                        null, null);
                for (Product prod : items.getItems()) 
                {
                	getSingleProduct(prod);
                	
                }
            }
        }	
	}
	
	
	//===================================================================
	// save single product
	// ===================================================================	 
	private void getSingleProduct(Product prod)
	{
		try{
			List<ProductOption> lpOpt = new ArrayList<ProductOption>();
			ProductOption optSize = new ProductOption();
			//lpOpt = porOpt.getOptions(prod.getProductCode());
			
			
			if(prod.getOptions() != null)
			{
				lpOpt = prod.getOptions();
				for(ProductOption op : lpOpt)
				{
				   if(op.getAttributeFQN().equals("Tenant~Size")) {optSize = op; }
				}
			} 
			
			// color option
			ProductOption opt = new ProductOption();
			opt.setAttributeFQN("Tenant~Color");	
			List<ProductOptionValue> lpProp = new ArrayList<ProductOptionValue>();
			ProductOptionValue pProp = new ProductOptionValue();
			AttributeVocabularyValue avvVal = new AttributeVocabularyValue();
			avvVal.setValue("Abalone");
			avvVal.setValueSequence(0);
			
			
			AttributeVocabularyValueLocalizedContent  cont = new AttributeVocabularyValueLocalizedContent();
			cont.setLocaleCode("en-US");
			
			cont.setStringValue("Abalone");
			List<AttributeVocabularyValueLocalizedContent> lcont = new ArrayList<AttributeVocabularyValueLocalizedContent>();
			lcont.add(cont);
			avvVal.setLocalizedContent(lcont);
			
			 		
			pProp.setAttributeVocabularyValueDetail(avvVal);
			pProp.setValue("Abalone");
			lpProp.add(pProp);
			
			opt.setValues(lpProp);
			 
			lpOpt.add(opt);
			
			
			//size			
			optSize.setAttributeFQN("Tenant~Size");	
			lpProp = new ArrayList<ProductOptionValue>();
			pProp = new ProductOptionValue();
			avvVal = new AttributeVocabularyValue();
			avvVal.setValue("11");
			//avvVal.setValueSequence(0);
			
			
			cont = new AttributeVocabularyValueLocalizedContent();
			cont.setLocaleCode("en-US");
			
			cont.setStringValue("11");
		    lcont = new ArrayList<AttributeVocabularyValueLocalizedContent>();
			lcont.add(cont);
			avvVal.setLocalizedContent(lcont);
			
			 		
			pProp.setAttributeVocabularyValueDetail(avvVal);
			pProp.setValue("11");
			lpProp.add(pProp);
			optSize.setValues(lpProp);	 
			lpOpt.add(optSize);
			prod.setOptions(lpOpt);
			
			porOpt.addOption(optSize, prod.getProductCode());
			prod  = productResource.updateProduct(prod, prod.getProductCode());	
			
			// ============ add variations ===================
			setProdVariations(prod, lpOpt);
			
			
			
			/*ProductVariationPagedCollection product;			
		    product  = pvrChild.getProductVariations(prod.getProductCode());
		    
		    ProductOption poFirst = lpOpt.get(0);
		    		    	
		    for (ProductVariation pv : product.getItems()) 
			{	
		    	String sVarKey = "2349004";
		    	pv.setVariationProductCode(prod.getProductCode() +  "-" + sVarKey);
		    	System.out.println(pv.getVariationProductCode());
		    	pv.setVariationExists(true);		    	
		    	pv.setVariationkey(sVarKey);
		    	
		    	ProductVariationDeltaPrice pvdpPrice = new ProductVariationDeltaPrice();
		    	pvdpPrice.setCreditValue(1.00);
		    	pvdpPrice.setCurrencyCode("USD");
		    	pvdpPrice.setMsrp(2.00);
		    	pvdpPrice.setValue(1.00);
		    	pv.setDeltaPrice(pvdpPrice);
		    	
		    	List<ProductVariationOption> lpvoOpt = pv.getOptions();
		    	ProductVariationOption pvoOpt = new ProductVariationOption();		    	
		    	pvoOpt.setAttributeFQN(lpOpt.get(0).getAttributeFQN());
		    	pvoOpt.setValue(lpOpt.get(0).getValues());		    	
		    	pvoOpt.setContent(lpOpt.get(0).getValues()
		    			       .get(0).getAttributeVocabularyValueDetail().getContent());
		    	
		    	pvrChild.updateProductVariation(pv, prod.getProductCode(), pv.getVariationkey());
			}
		     
		    prod  = productResource.updateProduct(prod, prod.getProductCode());		    
		    */
		    
		}
    	catch (Exception e) { e.printStackTrace(); }
	}
	
	//===================================================================
	// set Product Option and Variation
	// ===================================================================	 
	private void setProdVariations(Product prod, List<ProductOption> lpOpt)
	{
		try
		{	
			 
			ProductVariationPagedCollection pvcDlt = pvrChild.getProductVariations(prod.getProductCode(), 0, 200, null, null, null);
			ProductVariation pvVar = new ProductVariation(); 
			
			String sVarKey = null;
			
	    	List<ProductVariation> lItems =  pvcDlt.getItems();
	    	for(ProductVariation pv : lItems )
	    	{
	    		sVarKey = pv.getVariationkey();
	    		//pvrChild.deleteProductVariation(prod.getProductCode(), skey);
	    	}
			
			//set delta weight
			pvVar.setDeltaWeight(0.00);
			pvVar.setIsActive(true);
			pvVar.setIsOrphan(false);
			pvVar.setUpc("123456789012");
			pvVar.setVariationExists(true);
			
			pvVar.setVariationProductCode(prod.getProductCode() + "-1115555");
			pvVar.setVariationkey(sVarKey);
			
			//set delta price
			ProductVariationDeltaPrice pvdpPrice = new ProductVariationDeltaPrice();
	    	pvdpPrice.setCreditValue(1.00);
	    	pvdpPrice.setCurrencyCode("USD");
	    	pvdpPrice.setMsrp(2.00);
	    	pvdpPrice.setValue(1.00);
	    	pvVar.setDeltaPrice(pvdpPrice);
	    	
	    	// set variation options for color
	    	ProductVariationOption pvoOptColor = new ProductVariationOption();
	    	pvoOptColor.setAttributeFQN(lpOpt.get(0).getAttributeFQN());
	    	pvoOptColor.setValue(lpOpt.get(0).getValues());
	    	pvoOptColor.setContent(lpOpt.get(0).getValues().get(0)
	    			              .getAttributeVocabularyValueDetail().getContent());
	    	
	    	// set variation options for size
	    	ProductVariationOption pvoOptSize = new ProductVariationOption();
	    	pvoOptSize.setAttributeFQN(lpOpt.get(1).getAttributeFQN());
	    	pvoOptSize.setValue(lpOpt.get(1).getValues());
	    	pvoOptSize.setContent(lpOpt.get(1).getValues().get(0)
	    			              .getAttributeVocabularyValueDetail().getContent());
	    	
	    	List<ProductVariationOption> lpvoOpt =  new ArrayList<ProductVariationOption>();
	    	lpvoOpt.add(pvoOptColor);
	    	lpvoOpt.add(pvoOptSize);
	    	
	    	ProductVariationCollection pvcCol = new ProductVariationCollection();
	    	List<ProductVariation> lpvItem = new ArrayList<ProductVariation>();
	    	lpvItem.add(pvVar);
	    	pvcCol.setItems(lpvItem);
	    	
	    	
	    	
	    	
	    	pvrChild.updateProductVariations(pvcCol, prod.getProductCode());
		}
		catch (Exception e) { e.printStackTrace(); }
	}
	/**
	 * ===================================================================
	 * test
	 * ===================================================================	 */
	public static void main(String[] args) 
	{
		MozuProductOptions mprod = new MozuProductOptions();
		try {
			//mprod.getProducts();
			String sFilter = "ProductCode eq 2203051473311";
			//2203051473311-2349001
			System.out.println("sssss");
			mprod.getProductsByFilter(sFilter);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

