package com.test.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.io.*;
import java.math.*;

import com.mozu.api.contracts.productadmin.Attribute;
import com.mozu.api.contracts.productadmin.AttributeInProductType;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValue;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueInProductType;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueLocalizedContent;
import com.mozu.api.contracts.productadmin.ProductType;
import com.mozu.api.contracts.productadmin.ProductTypeCollection;
import com.mozu.api.resources.commerce.catalog.admin.attributedefinition.*;
import com.mozu.api.ApiContext;

/**
 * @author vrozen
 * @ttile Create new attribute in Mozu 
 */
public class GetAttrLabel  extends MozuInit
{
	private ApiContext apiContext = null;
	private AttributeResource attres = null;	
	private Attribute attrib = null;
	private List<AttributeVocabularyValue> lvocval = null;
	private ProductTypeResource prodType = null;
	private ProductTypeCollection prodcol = null;
	private AttributeInProductType aptOpt = null;
	private List<AttributeVocabularyValue> lvocValLoc = null;
	private AttributeVocabularyValue vocVal = null;
	private boolean bError = false;
	private String sError = "NONE";
	
	/** =======================================================
	 * Constructor - connect to mozu
	 * ======================================================= */
	public GetAttrLabel() 
	{
		try {
				apiContext = super.getApiCont();
				attres = new AttributeResource(apiContext);
				prodType = new ProductTypeResource(apiContext);
			} 
		    catch (Exception e)
		    {
		    	System.out.println("\n" + e.getMessage());
		    	bError = true;
		    	sError = e.getMessage();
		    }
	}
	
	//=======================================================
  	// set new attribute
  	//=======================================================
    public void rtvAttr(String sAttr)
    {
    	try 
    	{
    		attrib = attres.getAttribute(sAttr);
    		lvocval  =  attrib.getVocabularyValues();   
    		for(AttributeVocabularyValue vocval : lvocval)
    		{
    			System.out.println(vocval.getValueSequence()
    			 + " " + vocval.getValue()   					
    			);
    		}
    	}
    	catch (Exception e)
	    {
	    	System.out.println("\n" + e.getMessage());
	    	bError = true;
	    	sError = e.getMessage();
	    }
    }
	
    /** =======================================================
	 * test
	 * ======================================================= */
	public static void main(String[] args) 
	{
		GetAttrLabel crtattr = new GetAttrLabel();
		String sAttr = "Tenant~Color";
		
		crtattr.rtvAttr(sAttr);
	}

}
