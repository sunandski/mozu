package com.test.api;

import java.io.File;
import java.util.List;

import org.apache.commons.configuration.PropertiesConfiguration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mozu.api.ApiContext;
import com.mozu.api.MozuApiContext;
import com.mozu.api.MozuConfig;
import com.mozu.api.contracts.appdev.AppAuthInfo;
import com.mozu.api.contracts.commerceruntime.orders.Order;
import com.mozu.api.contracts.commerceruntime.orders.OrderCollection;
import com.mozu.api.contracts.core.UserAuthInfo;
import com.mozu.api.contracts.productadmin.Product;
import com.mozu.api.contracts.productadmin.ProductCollection;
import com.mozu.api.contracts.tenant.Site;
import com.mozu.api.contracts.tenant.Tenant;
import com.mozu.api.resources.commerce.OrderResource;
import com.mozu.api.resources.commerce.catalog.admin.ProductResource;
import com.mozu.api.resources.platform.TenantResource;
import com.mozu.api.security.AppAuthenticator;
import com.mozu.api.security.AuthenticationProfile;
import com.mozu.api.security.AuthenticationScope;
import com.mozu.api.security.UserAuthenticator;

public class MozuInit {
	
	public final static String CONFIG_PROPERTIES_FILENAME = "mozu_config.properties";
	
	// the following are defined properties for the SDK
	public final static String MOZU_BASE_URL = "base.url";
	public final static String SHARED_SECRET = "shared.secret";
	public final static String APP_ID = "app.id";
	public final static String TENANT_USERNAME = "username.tenant";
	public final static String TENANT_ID = "tenant.id";
	public final static String PASSWORD = "password";

	protected static PropertiesConfiguration configProps = null;

	private final static Integer MASTER_CATALOG_ID = Integer.valueOf(0);
	private com.mozu.api.ApiContext apiContext;
	
	/**
	 * ==============================================================================
	 * Constructor initialize communication with Mozu
	 * ==============================================================================
	 */
	public MozuInit() {
		try 
		{			
			init();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * ==============================================================================
	 * initialize communication with Mozu
	 * ==============================================================================
	 */	
	public void init() throws Exception 
	{	
		File f = new File(CONFIG_PROPERTIES_FILENAME);
		System.out.println(CONFIG_PROPERTIES_FILENAME + " is exists? " + f.exists());
		
		configProps = new PropertiesConfiguration(CONFIG_PROPERTIES_FILENAME);
		MozuConfig.setBaseUrl(configProps.getString(MOZU_BASE_URL));
		
		System.out.println("TenantId=" + configProps.getInt(TENANT_ID)
				+ "   User=" + configProps.getString(TENANT_USERNAME)
				+ "   Mozu_base_url=" + configProps.getString(MOZU_BASE_URL)
		);
		
		authenticateUser(AuthenticationScope.Tenant,
				configProps.getString(TENANT_USERNAME));
		TenantResource tenantResource = new TenantResource();
		Tenant tenant = tenantResource.getTenant(configProps.getInt(TENANT_ID));
		List<Site> sites = tenant.getSites();
		Site site = sites.get(0);
		Integer catalogId = site.getCatalogId();
		apiContext = new com.mozu.api.MozuApiContext(configProps.getInt(TENANT_ID),
				site.getId(), MASTER_CATALOG_ID, catalogId);		
	}
	// ==============================================================================
	// authenticate application 
	// ==============================================================================
	protected AppAuthInfo createAppAuthInfo(String appId, String sharedSecret) {
		AppAuthInfo appAuthInfo = new AppAuthInfo();
		appAuthInfo.setApplicationId(appId);
		appAuthInfo.setSharedSecret(sharedSecret);

		return appAuthInfo;
	}
	// ==============================================================================
	// authenticate user 
	// ==============================================================================
	protected AuthenticationProfile authenticateUser(AuthenticationScope scope,
			String userName) {
		String appId = configProps.getString(APP_ID);
		String sharedSecret = configProps.getString(SHARED_SECRET);

		AppAuthenticator.initialize(createAppAuthInfo(appId, sharedSecret));

		// Authorize user and get tenants
		UserAuthInfo userAuth = new UserAuthInfo();
		userAuth.setEmailAddress(userName);
		userAuth.setPassword(configProps.getString(PASSWORD));

		// Authorize user
		return UserAuthenticator.authenticate(userAuth, scope);
	}

	// ==============================================================================
	// get output variables 
	// ==============================================================================
	public ApiContext getApiCont(){ return apiContext; }
	
	
	// ==============================================================================
	// test this class 
	// ==============================================================================		
	public static void main(String[] args) 
	{
		System.out.println("start");
		MozuInit mi = new MozuInit();
	}
}
