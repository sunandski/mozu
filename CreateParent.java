package com.test.api; 

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.io.*;
import java.math.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mozu.api.contracts.commerceruntime.discounts.AppliedLineItemProductDiscount;
import com.mozu.api.contracts.commerceruntime.fulfillment.Shipment;
import com.mozu.api.contracts.commerceruntime.payments.Payment;
import com.mozu.api.contracts.location.Location;
import com.mozu.api.contracts.location.LocationCollection;
import com.mozu.api.contracts.productadmin.ProductOption;
import com.mozu.api.contracts.content.Document;
import com.mozu.api.contracts.core.Measurement;
import com.mozu.api.contracts.productadmin.LocationInventory;
import com.mozu.api.contracts.productadmin.LocationInventoryAdjustment;
import com.mozu.api.contracts.productadmin.LocationInventoryCollection;
import com.mozu.api.contracts.productadmin.Product;
import com.mozu.api.contracts.productadmin.ProductCategory;
import com.mozu.api.contracts.productadmin.ProductCollection;
import com.mozu.api.contracts.productadmin.ProductCost;
import com.mozu.api.contracts.productadmin.ProductInCatalogInfo;
import com.mozu.api.contracts.productadmin.ProductInventoryInfo;
import com.mozu.api.contracts.productadmin.ProductLocalizedContent;
import com.mozu.api.contracts.productadmin.ProductLocalizedImage;
import com.mozu.api.contracts.productadmin.ProductLocalizedSEOContent;
import com.mozu.api.contracts.productadmin.ProductOptionValue;
import com.mozu.api.contracts.productadmin.ProductPrice;
import com.mozu.api.contracts.productadmin.ProductProperty;
import com.mozu.api.contracts.productadmin.ProductPropertyValue;
import com.mozu.api.contracts.productadmin.ProductPublishingInfo;
import com.mozu.api.contracts.productadmin.ProductSupplierInfo;
import com.mozu.api.contracts.productadmin.ProductVariation;
import com.mozu.api.contracts.productadmin.ProductVariationDeltaPrice;
import com.mozu.api.contracts.productadmin.ProductVariationPagedCollection;
import com.mozu.api.contracts.productruntime.ProductImage;
import com.mozu.api.resources.commerce.admin.LocationResource;
import com.mozu.api.resources.commerce.catalog.admin.ProductResource;
import com.mozu.api.resources.content.documentlists.DocumentResource;
import com.mozu.api.ApiContext;
import com.mozu.api.MozuApiContext;
import com.mozu.api.resources.commerce.catalog.admin.products.LocationInventoryResource;
import com.mozu.api.resources.commerce.catalog.admin.products.ProductVariationResource;
import com.mozu.api.contracts.productadmin.ProductType;
import com.mozu.api.resources.commerce.catalog.admin.products.ProductPropertyResource;
import com.mozu.api.resources.commerce.catalog.admin.attributedefinition.ProductTypeResource;

public class CreateParent  extends MozuInit 
{
	private ApiContext apiContext;
	ProductResource productResource = null;
	DocumentResource docresImage = null;
	LocationResource locresInventory = null;
	LocationInventoryResource locinvresInventory = null;
	ProductPropertyResource propRes = null;
	private String sSite = "";
	private String [] sLoc = null;
	ProductTypeResource prodTyRes = null;
	Product product = null;
	ProductLocalizedContent plcLoc = null;
	String sError = null;
	boolean bError = false;

	public CreateParent() 
	{
		try 
		{  
			apiContext = super.getApiCont();
			
			productResource = new ProductResource(apiContext);
			docresImage = new DocumentResource(apiContext);
			locresInventory = new LocationResource(apiContext);
			locinvresInventory = new LocationInventoryResource(apiContext);
            propRes = new ProductPropertyResource(apiContext);
            prodTyRes = new ProductTypeResource(apiContext);
            plcLoc = new ProductLocalizedContent();
		} 
		catch (Exception e){e.printStackTrace();} 
	}
	/* ===================================================================== 
	 * set new product  
	 * ===================================================================== */
	public void setNewProduct(String sParent, String sProdType, String sProdTypeId, String sProdName
			, String sMsrp, String sPrice, String sSls, String sVenSty, String sCost, String sBrand
			, String [] sNormClr, String sGender)
	{
		System.out.println("CreateParent: Add/Update " + sParent);
		try 
		{
			bError = false;
			sError = null;
			boolean bNew = true; 
			try
			{
				product = productResource.getProduct(sParent);
				//productResource.deleteProduct(sParent);
				bNew = false;
			} 
			catch(Exception e){e.printStackTrace();}
			
			if(bNew)
			{
				product = new Product();		
				product.setProductCode(sParent);			
				product.setProductTypeId(Integer.parseInt(sProdTypeId));
				product.setProductUsage("Configurable");
				
				// set new product fulfillment				
				product.setFulfillmentTypesSupported(setNpFfl());
				
				product.setMasterCatalogId(1);
				product.setIsValidForProductType(true);
				
				// --------------
				// set meta tag key words
				product.setSeoContent(setNpMeta(sProdType));
								
				product.setHasConfigurableOptions(true);
				product.setHasStandAloneOptions(false);
				product.setIsVariation(false);
				product.setIsTaxable(true);
					
				// set new product inventory info								
				product.setInventoryInfo(setNpInventory());
					 
				product.setIsRecurring(false);				
				
				// set measurements				
				product.setPackageWeight(setNPMeasur("lbs", 1.00));
				product.setPackageLength(setNPMeasur("in", 1.00));
				product.setPackageHeight(setNPMeasur("in", 1.00));
				product.setPackageWidth(setNPMeasur("in", 1.00));
			}
			
			
			// set new product catalog
			product.setProductInCatalogs(setNpCatalog(sProdName));
			
			product.setContent(plcLoc);
			
			// set new product price				
			product.setPrice(setNpPrice(sMsrp, sPrice, sSls));
				
				
			// set NP supplier (vendor) info 
			product.setSupplierInfo(setNPSupplier(sVenSty, sCost));
				
			product.setIsPackagedStandAlone(false);
			product.setStandAlonePackageType("Custom");
			ProductPublishingInfo ppiPub = new ProductPublishingInfo();
			ppiPub.setPublishedState("Live"); //Pending???			
			product.setPublishingInfo(ppiPub);
					
			// set vendor
			setProdProp("tenant~brand", new String[]{sBrand} ); 
			// set normalized color
			setProdProp("tenant~NormalizedColor", sNormClr );			
			// set gender
			setProdProp("tenant~gender", new String[]{sGender} ); 
			 			 
			if(bNew)
			{
				productResource.addProduct(product);	
				
			}
			else
			{
				productResource.updateProduct(product, product.getProductCode());
			}
			
		}
		catch (Exception e)
		{
			bError = true;
			sError = e.getMessage();
			System.out.println("CreateParent:" + e.getMessage());			
		}
	}
	//=====================================================================
	// get existing product 
	//=====================================================================
	public void setProdProp(String sAttr, String [] sProp )
	{
		List<ProductProperty> lpProp = new ArrayList<ProductProperty>();
		ProductProperty pProp = new ProductProperty();
		pProp.setAttributeFQN(sAttr);
		
		List<ProductPropertyValue> lppVal = new ArrayList<ProductPropertyValue>();		
		
		for(int i=0; i < sProp.length ; i++)
		{
				ProductPropertyValue ppVal = new ProductPropertyValue();		
				ppVal.setValue(sProp[i]);
				lppVal.add(ppVal);				
		}
				
		pProp.setValues(lppVal);
		
		lpProp.add(pProp);				
		product.setProperties(lpProp);
	}
	//=====================================================================
	// get existing product 
	//=====================================================================
	public boolean getExistProduct(String sParent)
	{
		boolean bExist = false;
		try 
		{
			try
			{	
				product = productResource.getProduct(sParent);
				bExist = true;
			} 
			catch(Exception e){ }
		}	
		catch (Exception e){e.printStackTrace();}
		return bExist;
	}	
	//=====================================================================
	// set product Fulfillment 
	//=====================================================================
	private List<String> setNpFfl()
	{
		List<String> lFflType = new ArrayList<String>();
		lFflType.add("DirectShip");		
		return lFflType;
	}
	//=====================================================================
	// set new product catalog
	//=====================================================================
	private List<ProductInCatalogInfo> setNpCatalog(String sProdName)
	{
		List<ProductInCatalogInfo> lProdCatInfo = new ArrayList<ProductInCatalogInfo>();
		ProductInCatalogInfo prodCatInfo = new ProductInCatalogInfo();
		prodCatInfo.setCatalogId(1);
		prodCatInfo.setIsActive(true);
		prodCatInfo.setIsContentOverridden(false);
		prodCatInfo.setIsPriceOverridden(false);
		prodCatInfo.setIsseoContentOverridden(false);
		
		plcLoc.setLocaleCode("en-US");
		plcLoc.setProductName(sProdName);
		plcLoc.setProductShortDescription(sProdName);
		plcLoc.setProductFullDescription(sProdName);	 
		//plcLoc.setProductImages();
		
		prodCatInfo.setContent(plcLoc);
		
		List<ProductCategory> lProdCat = new ArrayList<ProductCategory>();
		ProductCategory pcCat = new ProductCategory();
		pcCat.setCategoryId(1);
		lProdCat.add(pcCat);
		prodCatInfo.setProductCategories(lProdCat); 
		
		lProdCatInfo.add(prodCatInfo);
		return lProdCatInfo; 
	}
	//=====================================================================
	// set new product price
	//=====================================================================
	private ProductPrice setNpPrice(String sMsrp, String sPrice, String sSls)
	{
		ProductPrice pp = new ProductPrice();
		pp.setMsrp( Double.valueOf(sMsrp));
		pp.setPrice(Double.valueOf(sPrice));			
		pp.setSalePrice(Double.valueOf(sSls));
		
		return pp;
	}
	//=====================================================================
	// set metatag key words
	//=====================================================================
	private ProductLocalizedSEOContent setNpMeta(String sKeywrd)
	{
		ProductLocalizedSEOContent plcSeo = new ProductLocalizedSEOContent();
		plcSeo.setLocaleCode("en-US");
		plcSeo.setMetaTagTitle(plcLoc.getProductName());
		plcSeo.setSeoFriendlyUrl("test-friendly-url");
		plcSeo.setMetaTagKeywords(sKeywrd);
		return plcSeo;
	}
	//=====================================================================
	// set new product inventory info
	//=====================================================================
	private ProductInventoryInfo setNpInventory()
	{
		ProductInventoryInfo piiInfo = new ProductInventoryInfo();
		piiInfo.setManageStock(true);
		piiInfo.setOutOfStockBehavior("HIDEPRODUCT"); //DisplayMessage, AllowBackOrder
		return piiInfo;
	}
	//=====================================================================
	// set NP supplier (vendor) info
	//=====================================================================
	private ProductSupplierInfo setNPSupplier(String sVenSty, String sCost)
	{
		ProductSupplierInfo psiSuppl = new ProductSupplierInfo();
		psiSuppl.setMfgPartNumber(sVenSty);
		ProductCost cost = new ProductCost();
		cost.setCost(Double.valueOf(sCost) );
		cost.setIsoCurrencyCode("USD");
		psiSuppl.setCost(cost);
		
		return psiSuppl;
	}
	//=====================================================================
	// set measurements
	//=====================================================================
	private Measurement setNPMeasur(String sUnit, double dValue)
	{
		Measurement meas  = new Measurement();
		meas.setUnit(sUnit);
		meas.setValue(dValue);
		return meas;
	}
	
	/* =====================================================================
	 * get error	
	 * ===================================================================== */
		public boolean isError(){ return bError; }
		public String getError(){ return sError; }	
	/* ===================================================================== 
	 * test only
	 * ===================================================================== */
	public static void main(String[] args) 
	{
		String [] sNClr = new String[]{"White", "Blue", "Green"};
		CreateParent crtpar = new CreateParent();
		crtpar.setNewProduct("9110068711600", "Apparel", "3", "another men's t-shirt v002", "26.48", "27.78", "0.00"
				, "xyz 987", "15.78", "The North Face", sNClr, "Women's" );
	}

}
