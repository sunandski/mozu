package com.test.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.io.*;
import java.math.*;

import com.mozu.api.contracts.productadmin.Attribute;
import com.mozu.api.contracts.productadmin.AttributeCollection;
import com.mozu.api.contracts.productadmin.AttributeInProductType;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValue;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueInProductType;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueLocalizedContent;
import com.mozu.api.contracts.productadmin.ProductType;
import com.mozu.api.contracts.productadmin.ProductTypeCollection;
import com.mozu.api.resources.commerce.catalog.admin.attributedefinition.*;
import com.mozu.api.ApiContext;

/**
 * @author vrozen
 * @ttile Create new attribute in Mozu 
 */
public class CreateAttrinProdType  extends MozuInit
{
	private ApiContext apiContext = null;
	private AttributeResource attres = null;	
	private Attribute attrib = null;
	private ProductTypeResource prodType = null;
	private ProductTypeCollection prodcol = null;
	private AttributeInProductType aptOpt = null;
	private List<AttributeVocabularyValue> lvocValLoc = null;
	private AttributeVocabularyValueInProductType vocVal = null;
	private List<AttributeVocabularyValueInProductType> lvocVal_inpt = null;
	private List<ProductType>  lprodty = null;	
	private ProductType ptype = null;	
	private List<AttributeInProductType> lopProdTyp = null;
	private int iOptNum = 0;	
	private List<AttributeVocabularyValue> lvocval = null;
	private boolean bError = false;
	private String sError = "NONE";
	
	/** =======================================================
	 * Constructor - connect to mozu
	 * ======================================================= */
	public CreateAttrinProdType() 
	{
		try {
				apiContext = super.getApiCont();
				attres = new AttributeResource(apiContext);
				prodType = new ProductTypeResource(apiContext);
			} 
		    catch (Exception e)
		    {
		    	System.out.println("\n" + e.getMessage());
		    	bError = true;
		    	sError = e.getMessage();
		    }
	}
	//=======================================================
	// retrieve product type list 
	//=======================================================
	public void rtvProdType(String sAttr)
	{
		try 
		{	    
			// retrieve product types and options
			prodcol = prodType.getProductTypes();
			
			List<ProductType>  lprodty = prodcol.getItems();
			for(ProductType pt : lprodty)
			{
				ptype = pt;			    
				System.out.println("Type=" + ptype.getName());
				
				if(!ptype.getName().equals("Base"))
				{
					lopProdTyp = ptype.getOptions();			
			
					iOptNum = -1;
					for(AttributeInProductType opt : lopProdTyp)
					{
						iOptNum++;
						if(opt.getAttributeFQN().toLowerCase().equals(sAttr.toLowerCase()))
						{
							aptOpt = opt;
							lvocVal_inpt = aptOpt.getVocabularyValues();
						
							rtvAttrList( sAttr );
				
							// update product type resource
							ptype.setOptions(lopProdTyp);
							System.out.println(" Updating Type =" + ptype.getName());
							prodType.updateProductType(ptype, ptype.getId());							
						
							break;
						}				
					}
				}
			}	
	    } 
		catch (Exception e)
		{
		   	System.out.println("\n" + e.getMessage());
		   	bError = true;
		   	sError = e.getMessage();
		}
	}
	//=======================================================
	// retrieve attribute list 
	//=======================================================
	public void rtvAttrList(String sAttr)
	{
		try
		{
			attrib = attres.getAttribute(sAttr);
			lvocval  =  attrib.getVocabularyValues();
			for(AttributeVocabularyValue v : lvocval)
	    	{
				System.out.println("  Color=" + v.getContent().getStringValue());
				vocVal = new AttributeVocabularyValueInProductType();    				
    			vocVal.setVocabularyValueDetail(v);    				
    			vocVal.setValue(v.getValue());
    			lvocVal_inpt.add(vocVal);
    			aptOpt.setVocabularyValues(lvocVal_inpt);    				
    			lopProdTyp.set(iOptNum, aptOpt);
	    	}					
		} 
		catch (Exception e)
		{
		   	System.out.println("\n" + e.getMessage());
		   	bError = true;
		   	sError = e.getMessage();
		}
		
	}
	
    //=======================================================
    // return error status 
    //=======================================================
    public boolean isInError(){return bError; }
    public String getError(){return sError; }
    
	/** =======================================================
	 * test
	 * ======================================================= */
	public static void main(String[] args) 
	{
		CreateAttrinProdType crtattr = new CreateAttrinProdType();		
		crtattr.rtvProdType("tenant~color");	
	
	}

}
