package com.test.api;

import java.util.List;
  












import org.apache.commons.configuration.PropertiesConfiguration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mozu.api.ApiContext;
import com.mozu.api.MozuApiContext;
import com.mozu.api.MozuConfig;
import com.mozu.api.contracts.appdev.AppAuthInfo;
import com.mozu.api.contracts.commerceruntime.orders.Order;
import com.mozu.api.contracts.commerceruntime.orders.OrderCollection;
import com.mozu.api.contracts.core.UserAuthInfo;
import com.mozu.api.contracts.location.Location;
import com.mozu.api.contracts.location.LocationCollection;
import com.mozu.api.contracts.productadmin.LocationInventory;
import com.mozu.api.contracts.productadmin.Product;
import com.mozu.api.contracts.productadmin.ProductCollection;
import com.mozu.api.contracts.tenant.Site;
import com.mozu.api.contracts.tenant.Tenant;
import com.mozu.api.resources.commerce.OrderResource;
import com.mozu.api.resources.commerce.admin.LocationResource;
import com.mozu.api.resources.commerce.catalog.admin.ProductResource;
import com.mozu.api.resources.platform.TenantResource;
import com.mozu.api.security.AppAuthenticator;
import com.mozu.api.security.AuthenticationProfile;
import com.mozu.api.security.AuthenticationScope;
import com.mozu.api.security.UserAuthenticator;
import com.mozu.api.resources.commerce.catalog.admin.products.LocationInventoryResource;


public class TestAPI {
	public final static String CONFIG_PROPERTIES_FILENAME = "mozu_config.properties";
	
	// the following are defined properties for the SDK
	public final static String MOZU_BASE_URL = "base.url";
	public final static String SHARED_SECRET = "shared.secret";
	public final static String APP_ID = "app.id";
	public final static String TENANT_USERNAME = "username.tenant";
	public final static String TENANT_ID = "tenant.id";
	public final static String PASSWORD = "password";

	protected static PropertiesConfiguration configProps = null;

	private final static Integer MASTER_CATALOG_ID = Integer.valueOf(0);
	private ApiContext apiContext;

	public void init() throws Exception {
		configProps = new PropertiesConfiguration(CONFIG_PROPERTIES_FILENAME);
		MozuConfig.setBaseUrl(configProps.getString(MOZU_BASE_URL));
		authenticateUser(AuthenticationScope.Tenant,
				configProps.getString(TENANT_USERNAME));
		TenantResource tenantResource = new TenantResource();
		Tenant tenant = tenantResource.getTenant(configProps.getInt(TENANT_ID));
		List<Site> sites = tenant.getSites();
		Site site = sites.get(0);
		Integer catalogId = site.getCatalogId();
		apiContext = new MozuApiContext(configProps.getInt(TENANT_ID),
				site.getId(), MASTER_CATALOG_ID, catalogId);

	}

	protected AppAuthInfo createAppAuthInfo(String appId, String sharedSecret) {
		AppAuthInfo appAuthInfo = new AppAuthInfo();
		appAuthInfo.setApplicationId(appId);
		appAuthInfo.setSharedSecret(sharedSecret);

		return appAuthInfo;
	}

	protected AuthenticationProfile authenticateUser(AuthenticationScope scope,
			String userName) {
		String appId = configProps.getString(APP_ID);
		String sharedSecret = configProps.getString(SHARED_SECRET);

		AppAuthenticator.initialize(createAppAuthInfo(appId, sharedSecret));

		// Authorize user and get tenants
		UserAuthInfo userAuth = new UserAuthInfo();
		userAuth.setEmailAddress(userName);
		userAuth.setPassword(configProps.getString(PASSWORD));

		// Authorize user
		return UserAuthenticator.authenticate(userAuth, scope);
	}

	public void showProducts() throws Exception {
		ProductResource productResource = new ProductResource(apiContext);
		ProductCollection productCollection = productResource.getProducts();
		List<Product> products = productCollection.getItems();
		Product firstProduct = null;
		// print product codes
		for (Product product : products) {
			System.out.println(product.getProductCode());
			if (firstProduct == null) {
				firstProduct = product;
			}
		}
		// print first product if any
		if (firstProduct != null) {
			ObjectMapper mapper = new ObjectMapper();
			System.out.println(mapper.writerWithDefaultPrettyPrinter()
					.writeValueAsString(firstProduct));
		}
	}

	public void showOrders() throws Exception {
		OrderResource orderResource = new OrderResource(apiContext);
		OrderCollection orderCollection = orderResource.getOrders();
		List<Order> orders = orderCollection.getItems();
		Order firstOrder = null;
		// print order ids
		for (Order order : orders) {
			System.out.println(order.getId());
			if (firstOrder == null) {
				firstOrder = order;
			}
		}   
		// print first order if any
		if (firstOrder != null) {
			ObjectMapper mapper = new ObjectMapper();
			System.out.println(mapper.writerWithDefaultPrettyPrinter()
					.writeValueAsString(firstOrder));
		}
	}
	
	

    public void showInventory() throws Exception {
        LocationResource lResource = new LocationResource(apiContext);
        LocationCollection lcollection = lResource.getLocations();
        List<Location> llist = lcollection.getItems();
        for (Location location : llist) {
            try {
                LocationInventoryResource resource = new LocationInventoryResource(apiContext);
                LocationInventory inventory = resource.getLocationInventory("4004059971500-1850850", "HQ");
                ObjectMapper mapper = new ObjectMapper();
                System.out.println("-------------------- INVENTORY --------------------");
                System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(inventory));

            } catch (Exception e) {
               e.printStackTrace();
            }
        }
    }



	public static void main(String[] args) 
	{
		TestAPI api = new TestAPI();
		try {
			api.init();
			api.showProducts();
			//api.showOrders();
			//api.showInventory();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
