package com.test.api;

import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.test.api.*;

import rciutility.RunSQLStmt;

public class ReplicateVolColors extends RunSQLStmt
{

	public ReplicateVolColors() 
	{
		try {
		CreateAttribute crtattr = new CreateAttribute();
		
		String sStmt = "with prdtyf as ("
           + " select pttype, ptid from Rci.MoPrdTy"
           + " group by pttype, ptid"
           + " order by pttype"
           + ")"
           + " select pttype, ptid, vcclr"
           + " from prdtyf, Rci.volclr"
           + " order by pttype, vcclr"		         
        ;
		
		System.out.println(sStmt);
		
		setPrepStmt(sStmt);
		ResultSet rs = runQuery();
		
		Date from = new Date();
		Date to = new Date();

		int i = 1;
		String SvType = "";
		while(readNextRecord())
		{
			Date frdt = new Date();
			
			String sType = getData("PtType").trim();
			String sColor =  getData("vcclr").trim();
			
			if(i==1 || i%100 == 0){ System.out.print(" " + i + " " + sType); }
			if(!SvType.equals(sType)){ System.out.println("\n" + sType); SvType = sType; }
			
			crtattr.setNewAttrinProdType("Tenant~Color", sType, sColor);
			
			i++;
		}
		
		rs.close();				
		disconnect();
		
		
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args) 
	{
		ReplicateVolColors rplvolclr = new ReplicateVolColors();
	}

}
