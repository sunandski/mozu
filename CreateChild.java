package com.test.api;

import java.util.*;
import java.io.*;
import java.math.*;

import com.mozu.api.contracts.productadmin.ProductOption;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValue;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueLocalizedContent;
import com.mozu.api.contracts.productadmin.Product;
import com.mozu.api.contracts.productadmin.ProductOptionValue;
import com.mozu.api.contracts.productadmin.ProductVariation;
import com.mozu.api.contracts.productadmin.ProductVariationCollection;
import com.mozu.api.contracts.productadmin.ProductVariationDeltaPrice;
import com.mozu.api.contracts.productadmin.ProductVariationOption;
import com.mozu.api.contracts.productadmin.ProductVariationPagedCollection;
import com.mozu.api.resources.commerce.catalog.admin.ProductResource;
import com.mozu.api.ApiContext;
import com.mozu.api.resources.commerce.catalog.admin.products.ProductVariationResource;

public class CreateChild   extends MozuInit 
{
	private ApiContext apiContext = null;
	private ProductResource productResource = null;
	private ProductVariationResource pvrChild = null;
	private String [] sLoc = null;
	private ProductOption optColor = null;
	private ProductOption optSize = null;
	private String sError = null;
	private boolean bError = false;
	private String sVarKey = null;
	private int iVarId = -1;
	private boolean bFirst = true;

	/* ===================================================================== 
	 * constructor - initialize api  
	 * ===================================================================== */
	public CreateChild()
	{  		
		try {
			apiContext = super.getApiCont();
			setResources();
		} 
		catch (Exception e) 
		{
			bError = true;
			sError = e.getMessage();
			System.out.println("CreateChild: " + e.getMessage());		
		}
	}
	 
	
	/* ===================================================================== 
	 * setup all required resources  
	 * ===================================================================== */
	private void setResources() throws Exception
	{
		productResource = new ProductResource(apiContext);
		pvrChild = new ProductVariationResource(apiContext);
		
		SASS_refresh_locations sassloc = new SASS_refresh_locations(apiContext);
		sassloc.setSASSLocation();
		sLoc = sassloc.getSASSLocation();
	}
	
	
	//===================================================================
	// save single product
	// ===================================================================	 
	public void setChild(String sParent, String sChild, String sColor, String sSize
			, String sUpc)
	{
		System.out.println("CreateChild: Add/Update " + sChild);
		
		bError = false;
		sError = null;
		try{
			Product prod = productResource.getProduct(sParent);
			List<ProductOption> lpOpt = new ArrayList<ProductOption>();
			optColor = new ProductOption();
			optSize = new ProductOption();
						
			if(prod.getOptions() != null)
			{
				lpOpt = prod.getOptions();
				for(ProductOption op : lpOpt)
				{
				   if(op.getAttributeFQN().equals("Tenant~Color")) {optColor = op; }
				   if(op.getAttributeFQN().equals("Tenant~Size")) {optSize = op; }
				   
				}
			} 
			
			// add only new options
			if(isOptionNew(prod, "Tenant~Color", sColor)) 
			{ 
				optColor = setNewOption(prod, "Tenant~Color", sColor);
			}
			
			if(isOptionNew(prod, "Tenant~Size", sSize))
			{				
				optSize = setNewOption(prod, "Tenant~Size", sSize);
			}
			
			
			lpOpt = new ArrayList<ProductOption>();
			lpOpt.add(optColor);
			lpOpt.add(optSize);	
			
			prod.setOptions(lpOpt);
			
			//porOpt.addOption(optSize, prod.getProductCode());
			prod  = productResource.updateProduct(prod, prod.getProductCode());
			
			
			// ============ add variations ===================
			setProdVariations(prod, sChild, lpOpt, sUpc, sColor, sSize);
		}
    	catch (Exception e) 
    	{
    		bError = true;
			sError = e.getMessage();
			System.out.println("CreateChild ERROR: " + e.getMessage());		
    	}
	}
	
	//===================================================================
	// set Product Option and Variation
	// ===================================================================	 
	private boolean isOptionNew(Product prod, String sAttr, String sValue) throws Exception
	{
		boolean bNew = true;			

		
		List<ProductOption> lpOpt = prod.getOptions();
		if(lpOpt != null)
		{
			for(ProductOption opt : lpOpt )
			{
				if(opt.getAttributeFQN().equals(sAttr))
				{
					List<ProductOptionValue> lpv = opt.getValues();
					for(ProductOptionValue pv : lpv)
					{
						if(pv.getValue().equals(sValue)){ bNew = false; break;}
					}
					break;
				}
			}
		}
		return bNew;
	}	
	//===================================================================
	// set Product Option and Variation
	// ===================================================================	 
	private ProductOption setNewOption(Product prod, String sAttr, String sValue) throws Exception
	{
		
		// exists options
		ProductOption poOpt = null;
		
		List<ProductOption> lpOpt = prod.getOptions();
		if(lpOpt != null)
		{
			for(ProductOption opt : lpOpt )
			{
				if(opt.getAttributeFQN().equals(sAttr))
				{
					poOpt = opt;
					break;
				}
			}
		}
		
		if(poOpt == null){ poOpt = new ProductOption(); }
		
		poOpt.setAttributeFQN(sAttr);	
		List<ProductOptionValue> lpProp = new ArrayList<ProductOptionValue>();			
		if(poOpt.getValues() != null)
		{
			lpProp = poOpt.getValues();
		}
		
		AttributeVocabularyValue avvVal = new AttributeVocabularyValue();
		AttributeVocabularyValueLocalizedContent  cont = new AttributeVocabularyValueLocalizedContent();
		List<AttributeVocabularyValueLocalizedContent> lcont = new ArrayList<AttributeVocabularyValueLocalizedContent>();
		
		ProductOptionValue pProp = new ProductOptionValue();
		poOpt.setAttributeFQN(sAttr);	
		
		lpProp = new ArrayList<ProductOptionValue>();
		if(poOpt.getValues() != null)
		{
			lpProp = poOpt.getValues();
		}
		pProp = new ProductOptionValue();
		avvVal = new AttributeVocabularyValue();
		avvVal.setValue(sValue);
		avvVal.setValueSequence(0);			
		
		cont.setLocaleCode("en-US");		
		cont.setStringValue(sValue);
	    lcont = new ArrayList<AttributeVocabularyValueLocalizedContent>();
		lcont.add(cont);
		
		avvVal.setLocalizedContent(lcont);		
		 		
		pProp.setAttributeVocabularyValueDetail(avvVal);
		pProp.setValue(sValue);
		lpProp.add(pProp);
		poOpt.setValues(lpProp);
		
		return poOpt;
	}
	
	
	
	//===================================================================
	// set Product Option and Variation
	// ===================================================================	 
		private void setProdVariations(Product prod, String sChild, List<ProductOption> lpOpt
				,String sUpc, String sColor, String sSize)  throws Exception
		{
			ProductVariationPagedCollection pvcDlt = pvrChild.getProductVariations(prod.getProductCode(), 0, 200, null, null, null);
			ProductVariation pvVar = new ProductVariation(); 
			
			
	    	List<ProductVariation> lItems =  pvcDlt.getItems();
	    	getVarId(lItems, sColor, sSize);
	    	
	    	//set delta weight
			pvVar.setDeltaWeight(0.00);
			pvVar.setIsActive(true);
			pvVar.setIsOrphan(false);
			pvVar.setUpc(sUpc);
			pvVar.setVariationExists(true);
			
			pvVar.setVariationProductCode(sChild);
			
			pvVar.setVariationkey(sVarKey);			
			
			//set delta price
			ProductVariationDeltaPrice pvdpPrice = new ProductVariationDeltaPrice();
	    	pvdpPrice.setCreditValue(1.00);
	    	pvdpPrice.setCurrencyCode("USD");
	    	pvdpPrice.setMsrp(2.00);
	    	pvdpPrice.setValue(1.00);
	    	pvVar.setDeltaPrice(pvdpPrice);
	    	
	    	// set variation options for color
	    	ProductVariationOption pvoOptColor = new ProductVariationOption();
	    	pvoOptColor.setAttributeFQN(lpOpt.get(0).getAttributeFQN());
	    	pvoOptColor.setValue(lpOpt.get(0).getValues());
	    	pvoOptColor.setContent(lpOpt.get(0).getValues().get(0)
	    			              .getAttributeVocabularyValueDetail().getContent());
	    	
	    	// set variation options for size
	    	ProductVariationOption pvoOptSize = new ProductVariationOption();
	    	pvoOptSize.setAttributeFQN(lpOpt.get(1).getAttributeFQN());
	    	pvoOptSize.setValue(lpOpt.get(1).getValues());
	    	pvoOptSize.setContent(lpOpt.get(1).getValues().get(0)
	    			              .getAttributeVocabularyValueDetail().getContent());
	    	
	    	List<ProductVariationOption> lpvoOpt =  new ArrayList<ProductVariationOption>();
	    	lpvoOpt.add(pvoOptColor);
	    	lpvoOpt.add(pvoOptSize);
	    	
	    	ProductVariationCollection pvcCol = new ProductVariationCollection();
	    	if(bFirst)
	    	{ 
	    		lItems = new ArrayList<ProductVariation>();
	    		lItems.add(pvVar);
	    		System.out.println("CreateChild: added first new  variation");
	    	}
	    	else 
	    	{ 
	    		
	    		if(iVarId < 0)
	    		{ 
	    			lItems.add(pvVar); 
	    		}
	    		else 
	    		{ 
	    			lItems.set(iVarId, pvVar); 
	    		}
	    	}
	    	
	    	pvcCol.setItems(lItems);
	    		    	
	    	pvrChild.updateProductVariations(pvcCol, prod.getProductCode());
		}
	//===================================================================
	// set Product Option and Variation
	// ===================================================================	
	private int getVarId(List<ProductVariation> lItems, String sColor, String sSize)
	{
		int iId = -1;		
		int i = 0;
		
		String vk = null;
		
    	bFirst = true;
    	
		boolean bFind = false; 
		
		for(ProductVariation pv : lItems )
    	{
			if(pv.getVariationExists())
			{
				bFirst = false;
				
				List<ProductVariationOption> lpvOpt = pv.getOptions();
				String clr = null;
				String siz = null;
				for(ProductVariationOption opt : lpvOpt)
				{			    	
					if(opt.getAttributeFQN().equals("Tenant~Color") && opt.getValue().toString().equals(sColor))
					{ 
						clr = opt.getValue().toString(); 
					}
					if(opt.getAttributeFQN().equals("Tenant~Size") && opt.getValue().toString().equals(sSize))
					{ 
						siz = opt.getValue().toString(); 
					}				
				}
				if(clr != null && siz != null)
				{  
					bFind = true; 
					iId = i; 
					sVarKey = pv.getVariationkey();
					break;					
				}				
			}
			else
			{
				vk = pv.getVariationkey();
			}
			i++;
    	}
		  
		if(!bFind) { sVarKey = vk;}
		
		return iId;
	}
	/* =====================================================================
	 * get error	
	 * ===================================================================== */
	public boolean isError(){ return bError; }
	public String getError(){ return sError; }	
	/* ===================================================================== 
	 * test only  
	 * ===================================================================== */
	public static void main(String[] args) 
	{
		CreateChild crtchi = new CreateChild();	
		crtchi.setChild("9110067811500", "9110067811500-0019004", "Black", "Medium_003", "400089226191");
		boolean bError = crtchi.isError();
		String sError = crtchi.getError();
		if(bError){System.out.println(sError);}
	}

}
