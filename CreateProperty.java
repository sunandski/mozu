/**
 * 
 */
package com.test.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.io.*;
import java.math.*;

import com.mozu.api.contracts.productadmin.Attribute;
import com.mozu.api.contracts.productadmin.AttributeInProductType;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValue;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueInProductType;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueLocalizedContent;
import com.mozu.api.contracts.productadmin.ProductType;
import com.mozu.api.contracts.productadmin.ProductTypeCollection;
import com.mozu.api.resources.commerce.catalog.admin.attributedefinition.*;
import com.mozu.api.ApiContext;
   
/**
 * @author vrozen
 * @ttile Create new attribute in Mozu 
 */
public class CreateProperty  extends MozuInit
{
	private ApiContext apiContext = null;
	private AttributeResource attres = null;
	private Attribute attr = null;
	private ProductTypeResource ProdTypeRes = null;
	private ProductTypeCollection ptcBase = null;
	private AttributeInProductType aptOpt = null;
	private String sError = "NONE";
	private boolean bError = false;
	
	/** =======================================================
	 * Constructor - connect to mozu
	 * ======================================================= */
	public CreateProperty()   
	{
		try {
				apiContext = super.getApiCont();
				attres = new AttributeResource(apiContext);
				ProdTypeRes = new ProductTypeResource(apiContext);
			} 
		    catch (Exception e)
		    {
		    	System.out.println("\n" + e.getMessage());
		    	bError = true;
		    	sError = e.getMessage();
		    }
	}
	
	//=======================================================
	// create new Property (brand, gender) 
	//=======================================================
    public void setNewProp(String sAttr, String sProdTypeRes, String sValue)
    {
    	sError = "NONE";
    	bError = false;    	
    	
    	try {
    		// set new attribute
    		Attribute attr = attres.getAttribute(sAttr);	
			String sAttrCode = attr.getAttributeCode();
			
			Attribute attrib = attres.getAttribute(sAttr);
			sAttr = attrib.getAttributeFQN();		
			
			aptOpt = new AttributeInProductType();
			aptOpt.setAttributeFQN(attr.getAttributeFQN());
			aptOpt.setIsInheritedFromBaseType(false);
			aptOpt.setAttributeDetail(attr);
			aptOpt.setIsHiddenProperty(false);
			aptOpt.setIsMultiValueProperty(false);
			aptOpt.setIsRequiredByAdmin(false);
			aptOpt.setOrder(0);			
				
			AttributeVocabularyValueLocalizedContent vocValLocolized = new AttributeVocabularyValueLocalizedContent();				
			vocValLocolized.setStringValue(sValue);
			vocValLocolized.setLocaleCode("en-US");
			
			AttributeVocabularyValue vocValLoc = new AttributeVocabularyValue();
			vocValLoc.setValue(sValue);
			vocValLoc.setValueSequence(2736);
            vocValLoc.setContent(vocValLocolized); 
            
            AttributeVocabularyValueInProductType vocVal = new AttributeVocabularyValueInProductType();
			vocVal.setValue(sValue);
			vocVal.setOrder(0);
			vocVal.setVocabularyValueDetail(vocValLoc);
			
			List<AttributeVocabularyValueInProductType> lvocval  =  new ArrayList<AttributeVocabularyValueInProductType>();
			lvocval.add(vocVal);
			
			aptOpt.setVocabularyValues(lvocval);
			
			List<AttributeVocabularyValue> lvocValLoc = attrib.getVocabularyValues();
			lvocValLoc.add(vocValLoc);
			attrib.setVocabularyValues(lvocValLoc);
			
			attres.updateAttribute(attrib, sAttr);
			
			// update product type
			ProductType ptBase = ProdTypeRes.getProductType(1);
			
			List<AttributeInProductType> laptBase = ptBase.getProperties();
			for(AttributeInProductType aptBase : laptBase)
			{
				if(aptBase.getAttributeFQN().equals(sAttr))
				{
					System.out.println(aptBase.getAttributeFQN());
					List<AttributeVocabularyValueInProductType> lavvtBrand = aptBase.getVocabularyValues();
					lavvtBrand.add(vocVal);
					break;
				}
			}
			
			ptBase.setProperties(laptBase);
			ProdTypeRes.updateProductType(ptBase, 1);
			
			System.out.println("brand added to PT " + sValue);
						
			ptBase = ProdTypeRes.getProductType(1);
			laptBase = ptBase.getProperties();
			
			/*for(AttributeInProductType aptBase : laptBase)
			{
				if(aptBase.getAttributeFQN().equals("Tenant~Brand"))
				{
					List<AttributeVocabularyValueInProductType> lavvtBrand = aptBase.getVocabularyValues();
					for(AttributeVocabularyValueInProductType avvtBrand: lavvtBrand)
					{
						System.out.println(" " + avvtBrand.getValue());
					}
				}
			}
			*/
		} 
	    catch (Exception e) 
	    {
	    	System.out.println("\n" + e.getMessage());
	    	bError = true;
	    	sError = e.getMessage();
	    }
    }
    
  //=======================================================
  // return error status 
  //=======================================================
  public boolean isInError(){return bError; }
  public String getError(){return sError; }
    
	/** =======================================================
	 * test
	 * ======================================================= */
	public static void main(String[] args) 
	{
		CreateProperty crtprop = new CreateProperty();
		crtprop.setNewProp("Tenant~Gender", "Base", "Kid_001");
	}

}
