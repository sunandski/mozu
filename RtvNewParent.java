package com.test.api;

import mozu_com.MozuItemLog;
import rciutility.ConnDataQueueObject;
import rciutility.CallAs400SrvPgmSup;

public class RtvNewParent {
	
	private String sSite=null; 
	private String sParent=null;
	private String sCode=null;
	private String sProdName=null;
	private String sMfg=null;	
	private String sMsrp=null;
	private String sPrice=null;
	private String sSls=null;
	private String sHide=null;
	private String sGender=null;
	private String sMap=null; 
	private String sMapExpDt=null;
	private String sVenSty=null;	
	private String sProdType=null; 
	private String sProdTypeId=null; 
	private String sCost=null;
	private String sClrNm = null;
	private String sSizNm = null;	
	private int iNumOfNorm = 0;
	private String [] sNormClr = null;	
	private String sUpc = null;
	private String sUser = null;
	
	int [] iLng = {  10, 1, 13, 50, 50, 13, 13, 13, 1, 50, 13, 10, 15, 100, 5, 13, 5, 30 * 20, 10 };	
	int [] iLng1 = { 10, 1, 21, 100, 5, 100, 100, 12, 10 };	
	
	
	CallAs400SrvPgmSup srvpgm = new CallAs400SrvPgmSup();
	ConnDataQueueObject dqParent = null;	
	CreateParent crtpar = null;
	CreateChild crtchi = null;
	MozuItemLog svitmlog = new MozuItemLog();	

	public RtvNewParent() 
	{		
		String sData = "BEGINNING";
		
		dqParent = new ConnDataQueueObject("TMPLIB","MONEWITM");	
				
		// create new parent
		crtpar = new CreateParent();
		crtchi = new CreateChild();	  
		
		// data queue
		while(!sData.equals("END"))
		{			
			sData = dqParent.read();
			if(!sData.equals("END"))
			{
				// parse parent data
				if(srvpgm.parseOutValue(sData, 1, iLng).trim().equals("1"))
				{					
					parseParent(sData);
					setnewparent();
				}
				// parse child data
				else
				{
					parseChild(sData);
					setnewchild();
				}
			} 
			else
			{
				System.out.println("Endded by request.");
			}
		}
	}
	
	//=====================================================================
	// parse data from data queue
	//=====================================================================
	private void parseParent(String sData)
	{
		sSite = srvpgm.parseOutValue(sData, 0, iLng).trim();
		sParent = srvpgm.parseOutValue(sData, 1, iLng).trim();
		sCode = srvpgm.parseOutValue(sData, 2, iLng).trim();		
		sProdName = srvpgm.parseOutValue(sData, 3, iLng).trim();
		sMfg = srvpgm.parseOutValue(sData, 4, iLng).trim(); 
		sMsrp = srvpgm.parseOutValue(sData, 5, iLng).trim();
		sPrice = srvpgm.parseOutValue(sData, 6, iLng).trim();
		sSls = srvpgm.parseOutValue(sData, 7, iLng).trim();
		sHide = srvpgm.parseOutValue(sData, 8, iLng).trim(); 
		sGender = srvpgm.parseOutValue(sData, 9, iLng).trim(); 
		sMap = srvpgm.parseOutValue(sData, 10, iLng).trim();
		sMapExpDt = srvpgm.parseOutValue(sData, 11, iLng).trim(); 
		sVenSty = srvpgm.parseOutValue(sData, 12, iLng).trim();
		sProdType = srvpgm.parseOutValue(sData, 13, iLng).trim(); 
		sProdTypeId = srvpgm.parseOutValue(sData, 14, iLng).trim(); 
		sCost = srvpgm.parseOutValue(sData, 15, iLng).trim();   

		// normalize color list
		iNumOfNorm = Integer.parseInt(srvpgm.parseOutValue(sData, 16, iLng));
		sNormClr = srvpgm.parseArray(srvpgm.parseOutValue(sData, 17, iLng), iNumOfNorm, 30, true);
		
		sUser = srvpgm.parseOutValue(sData, 18, iLng).trim();
	}
	
	//=====================================================================
	// set new parent
	//=====================================================================
	private void setnewparent()
	{
		crtpar.setNewProduct(sCode, sProdType, sProdTypeId, sProdName, sMsrp, sPrice, sSls, sVenSty
				, sCost, sMfg, sNormClr, sGender);
		if(crtpar.isError())
		{				
			StringBuffer sbParam = new StringBuffer();
		    sbParam.append(srvpgm.setParamString(sSite, 10));
		    sbParam.append(srvpgm.setParamString(sParent, 1));    
		    sbParam.append(srvpgm.setParamString(sCode, 21));		    
			String sError = sbParam.toString() + "Error: " + crtpar.getError().trim();	
			svitmlog.saveLog("SASS", "1", sCode.substring(0, 4), sCode.substring(4, 9)
			  , sCode.substring(9), "0", "0", "UPLOAD_PARENT", "Y", sError, sUser);
		}
	}		
	//=====================================================================
		// parse data from data queue
		//=====================================================================
		private void parseChild(String sData)
		{
			sSite = srvpgm.parseOutValue(sData, 0, iLng1).trim();
			sParent = srvpgm.parseOutValue(sData, 1, iLng1).trim();
			sCode = srvpgm.parseOutValue(sData, 2, iLng1).trim();
			sProdType = srvpgm.parseOutValue(sData, 3, iLng1).trim(); 
			sProdTypeId = srvpgm.parseOutValue(sData, 4, iLng1).trim(); 
			sClrNm = srvpgm.parseOutValue(sData, 5, iLng1).trim();
			sSizNm = srvpgm.parseOutValue(sData, 6, iLng1).trim(); 
			sUpc = srvpgm.parseOutValue(sData, 7, iLng1).trim();
			sUser = srvpgm.parseOutValue(sData, 8, iLng1).trim();
		}
		
		//=====================================================================
		// set new parent
		//=====================================================================
		private void setnewchild()
		{
			crtchi.setChild(sCode.substring(0,13), sCode, sClrNm, sSizNm, sUpc);
			if(crtchi.isError())    
			{				
				StringBuffer sbParam = new StringBuffer();
			    sbParam.append(srvpgm.setParamString(sSite, 10));
			    sbParam.append(srvpgm.setParamString(sParent, 1));
			    sbParam.append(srvpgm.setParamString(sCode, 21));
			    String sError = sbParam.toString() + "Error: " + crtchi.getError().trim();
			    svitmlog.saveLog("SASS", "2", sCode.substring(0, 4), sCode.substring(4, 9)
				 , sCode.substring(9, 13), sCode.substring(14,17), sCode.substring(17)
				 , "UPLOAD_CHILD", "Y", sError, sUser);
			}
			else    
			{
				svitmlog.saveLog("SASS", "2", sCode.substring(0, 4), sCode.substring(4, 9)
				 , sCode.substring(9, 13), sCode.substring(14, 17), sCode.substring(17)
				 , "UPLOAD_CHILD", " ", " ", sUser);       
			}
		}
 	//=====================================================================
	// test
	//=====================================================================
	public static void main(String[] args) 
	{
		RtvNewParent rtvnpar = new RtvNewParent(); 
	}
}
