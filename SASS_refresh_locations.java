package com.test.api;

import java.io.*;
import java.util.*;

import com.mozu.api.contracts.location.Location;
import com.mozu.api.contracts.location.LocationCollection;
import com.mozu.api.resources.commerce.admin.LocationResource;
import com.mozu.api.ApiContext;
import com.mozu.api.MozuApiContext;

public class SASS_refresh_locations
{
	
	public final static String SASS_loc_Prop_File = "sass_locations.properties";
	
	private ApiContext apiContext;
	private Vector vCode = new Vector();
	private String [] sLoc = null;

	
	
	public SASS_refresh_locations(ApiContext apicont) 
	{
		try 
		{  
			apiContext = apicont;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}
	/**
	 * ====================================================================
	 * Refresh sun and ski locations(stores)
	 * ====================================================================
	 */
    public void resasslocations()
    {    	
    	try 
    	{
    		LocationResource lResource = new LocationResource(apiContext);
    		LocationCollection lcollection;
		
			lcollection = lResource.getLocations();
		
			List<Location> llist = lcollection.getItems();
			int iloc = 1;
			for (Location location : llist) 
			{
				String sCode = location.getCode();
				System.out.println(iloc + ". location=" + sCode);
				vCode.add(sCode);				
				iloc++;
			}        
			loadProp();
    	} 
    	catch (Exception e) {	System.out.println( e.getMessage() ); }
    }
    //====================================================================    
    // load location in properties file 
    //====================================================================
    private void loadProp()
    {
    	try {
            Properties props = new Properties();
            
            for(int i=0; i < vCode.size(); i++)
            {
                String sCode = (String) vCode.get(i); 
            	String loc = "location_" + i;
            	props.setProperty(loc, sCode);
            }
            
            File f = new File(SASS_loc_Prop_File);
            OutputStream out = new FileOutputStream( f );
            props.store(out, "");
            out.close();           
        }
        catch (Exception e ) {
            e.printStackTrace();
        }
    }
    //====================================================================    
    // read properties file  
    //====================================================================    
    public void setSASSLocation() 
    {
    	try {    		
    		Properties props = new Properties();
    		InputStream is = getClass().getClassLoader().getResourceAsStream(SASS_loc_Prop_File);
    		props.load(is);
    		
    		int iLine = props.size();
    		sLoc = new String[iLine];
    		
    		Set<Object> keys = props.keySet();
    		int i = 0;
    		for(Object k : keys)
    		{
    			String sKey = (String) k;
    			sLoc[i] = props.getProperty( sKey );
    			//System.out.println(sKey + " " + sLoc[i]);
    			i++;
    		}
    	} 
    	catch (Exception e ) {
            e.printStackTrace();
        }
    }

    /** -------------------------- export values --------------------------------------- */
    public String [] getSASSLocation(){ return sLoc;}
    
    /** -------------------- end of export values -------------------------------------- */
    
    /**
	 * ====================================================================
	 * test only!!!
	 * ====================================================================
	 */
	public static void main(String[] args) 
	{
		MozuInit init = new MozuInit();
		ApiContext apiContext = init.getApiCont();
		
		SASS_refresh_locations sassloc = new SASS_refresh_locations(apiContext);
		try {
			//sassloc.resasslocations();
			sassloc.setSASSLocation();
			String [] sLoc = sassloc.getSASSLocation();
			
			for(String s : sLoc )
			{
				System.out.println(s);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
