package com.test.api;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.io.*;
import java.math.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mozu.api.contracts.commerceruntime.discounts.AppliedLineItemProductDiscount;
import com.mozu.api.contracts.commerceruntime.fulfillment.Shipment;
import com.mozu.api.contracts.commerceruntime.payments.Payment;
import com.mozu.api.contracts.location.Location;
import com.mozu.api.contracts.location.LocationCollection;
import com.mozu.api.contracts.productadmin.ProductOption;
import com.mozu.api.contracts.content.Document;
import com.mozu.api.contracts.productadmin.LocationInventory;
import com.mozu.api.contracts.productadmin.LocationInventoryCollection;
import com.mozu.api.contracts.productadmin.Product;
import com.mozu.api.contracts.productadmin.ProductCategory;
import com.mozu.api.contracts.productadmin.ProductCollection;
import com.mozu.api.contracts.productadmin.ProductInCatalogInfo;
import com.mozu.api.contracts.productadmin.ProductInventoryInfo;
import com.mozu.api.contracts.productadmin.ProductLocalizedImage;
import com.mozu.api.contracts.productadmin.ProductOptionValue;
import com.mozu.api.contracts.productadmin.ProductProperty;
import com.mozu.api.contracts.productadmin.ProductPropertyValue;
import com.mozu.api.contracts.productadmin.ProductPublishingInfo;
import com.mozu.api.contracts.productadmin.ProductVariation;
import com.mozu.api.contracts.productadmin.ProductVariationPagedCollection;
import com.mozu.api.contracts.productruntime.ProductImage;
import com.mozu.api.resources.commerce.admin.LocationResource;
import com.mozu.api.resources.commerce.catalog.admin.ProductResource;
import com.mozu.api.resources.content.documentlists.DocumentResource;
import com.mozu.api.ApiContext;
import com.mozu.api.MozuApiContext;
import com.mozu.api.resources.commerce.catalog.admin.products.LocationInventoryResource;
import com.mozu.api.resources.commerce.catalog.admin.products.ProductVariationResource;



public class MozuProducts extends MozuInit 
{

	private String sSite = "2659";// ???? how to get it from properties file
	String sProd = " ";
	String sBaseProd = " ";
	String sName = "Y";
	String sFullDesc = "Y";
	String sShortDesc = "Y";   
	String sProdImg = " ";
	String sCateg = "Y";	
	String sMsrp = "0";
	String sMap = "0";
	String sPrice = "0";
	String sSlsPrice = "0";	
	String sWeight = "0";
	String sOnHand = "0";
	Vector vOptNm = new Vector();
	Vector vOptLst = new Vector();
	Vector vChild = new Vector();
	Vector vChildLoc = new Vector();
	Vector vChildInv = new Vector();
	
	private ApiContext apiContext;
	
	private String [] sLoc = null;
	
	/**
	 * ===================================================================
	 * Constructor - initialize connection via super class
	 * =================================================================== */
	public MozuProducts() 
	{
		try 
		{  
			System.out.println("MozuProducts init();");
			init();
			apiContext = super.getApiCont();
			SASS_refresh_locations sassloc = new SASS_refresh_locations(apiContext);
			sassloc.setSASSLocation();
			sLoc = sassloc.getSASSLocation();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}
	
	/**
	 * ===================================================================
	 * add product
	 * =================================================================== */
	public void addProducts() throws Exception 
	{
		ProductResource productResource = new ProductResource(apiContext);
	}
	/**
	 * ===================================================================================
	 * 
	 */
    public void showAllProducts() throws Exception {
        Integer maxCountPerPage = 50;
        ProductResource resource = new ProductResource(apiContext);
        ProductCollection collection = resource.getProducts();
        Integer total = collection.getTotalCount();

        if (total.intValue() > 0) {
            int totalPages = (int) Math.ceil(total.doubleValue() / maxCountPerPage.doubleValue());
            for (int i = 0; i < totalPages; i++) {
                int startIndex = i * maxCountPerPage;
                ProductCollection items = resource.getProducts(startIndex, maxCountPerPage, null, null, null, null,
                        null, null);
                for (Product item : items.getItems()) {
                }
            }
        }
    }

	/**
	 * ===================================================================
	 * get product
	 * =================================================================== */
	public void getProducts() throws Exception 
	{
		Integer maxCountPerPage = 50;
		ProductResource productResource = new ProductResource(apiContext);
		ProductCollection productCollection = productResource.getProducts();
		Integer total = productCollection.getTotalCount();
		if (total.intValue() > 0) {
            int totalPages = (int) Math.ceil(total.doubleValue() / maxCountPerPage.doubleValue());
            int iCount = 1;
            for (int i = 0; i < totalPages; i++) {
                int startIndex = i * maxCountPerPage;
                ProductCollection items = productResource.getProducts(startIndex, maxCountPerPage, null, null, null, null,
                        null, null);
                for (Product prod : items.getItems()) 
                {
                	getSingleProduct(prod, iCount++);
                	
                }
            }
        }	
	}
	
	/**
	 * ===================================================================
	 * get product
	 * =================================================================== */
	public void getProductsByFilter(String sFilter) throws Exception 
	{
		Integer maxCountPerPage = 50;
		ProductResource productResource = new ProductResource(apiContext);
		ProductCollection productCollection = productResource
				.getProducts(null, null, null, sFilter, null, null, null, null);
		Integer total = productCollection.getTotalCount();
		if (total.intValue() > 0) {
            int totalPages = (int) Math.ceil(total.doubleValue() / maxCountPerPage.doubleValue());
            int iCount = 1;
            for (int i = 0; i < totalPages; i++) {
                int startIndex = i * maxCountPerPage;
                ProductCollection items = productResource.getProducts(startIndex, maxCountPerPage, null, sFilter, null, null,
                        null, null);
                for (Product prod : items.getItems()) 
                {
                	getSingleProduct(prod, iCount++);
                	
                }
            }
        }	
	}
	
	
	//===================================================================
	// save single product
	// ===================================================================	 
	private void getSingleProduct(Product prod, int iCount)
	{
		try{				
			sSite = "2659";// ???? how to get it from properties file
			sProd = prod.getProductCode();
			sBaseProd = prod.getBaseProductCode();
			System.out.println(iCount + ".  BaseProd=" + sBaseProd + " sProd=" + sProd);
			sName = "Y";
			if(prod.getContent().getProductName() == null){ sName = "N"; }
			sFullDesc = "Y";
			if(prod.getContent().getProductFullDescription() == null){ sFullDesc = "N"; }
			sShortDesc = "Y";				
			if(prod.getContent().getProductShortDescription() == null){sShortDesc = "N";}
			
			// get product image properties
			sProdImg = getImgAvail(prod);
			
			sCateg = "Y";				
			if(prod.getProductInCatalogs() == null ) {  sCateg = "N"; }				
			
			sMsrp = "0";
			if(prod.getPrice().getMsrp() != null)
			{					
				sMsrp = BigDecimal.valueOf(prod.getPrice().getMsrp()).toString();
			}
			
			sMap = "0";
			if(prod.getPrice().getMap() != null)
			{
				sMap = BigDecimal.valueOf(prod.getPrice().getMap()).toString();
			}
			
			sPrice = "0";
			if(prod.getPrice().getPrice()  != null)
			{
				sPrice = BigDecimal.valueOf(prod.getPrice().getPrice()).toString();
			}
			sSlsPrice = "0";
			if(prod.getPrice().getSalePrice() != null){
				sSlsPrice = BigDecimal.valueOf(prod.getPrice().getSalePrice()).toString();
			}			
			
			// get product Child/variations
			getProdChild();
			
			// get product options
			getProdOpt(prod);						
			
			// get all properties - now get weight only
			getProdProp(prod);			
		}
    	catch (Exception e) { e.printStackTrace(); }
	}
	//===================================================================
	// get product Child/variations
	//===================================================================
	private void getProdChild()
	{		
		try {
			ProductVariationResource productResource = new ProductVariationResource(apiContext);
			ProductVariationPagedCollection product;
		
			product = productResource.getProductVariations(sProd);
			for (ProductVariation pv : product.getItems()) 
			{				
				System.out.println(pv.getVariationProductCode());
				getInventory(pv.getVariationProductCode());
			}
		} 
		catch (Exception e) { e.printStackTrace();}
	}
	//===================================================================
	// get product options
	// ===================================================================	 
	private void getProdOpt(Product prod)  throws Exception
	{
		List<ProductOption> lOptions = prod.getOptions();
		if(lOptions != null)
		{
			for(ProductOption option : lOptions)
			{
				vOptNm.add(option.getAttributeFQN());
				List<ProductOptionValue> lOptVal = option.getValues();
				Vector vVal  = new Vector();
				
				if(lOptVal != null)
				{						 
					for(ProductOptionValue optval : lOptVal)
					{
						vVal.add(optval.getValue());
						System.out.println(option.getAttributeFQN() + " = " + optval.getValue());
					}
				}
				vOptLst.add(vVal);
				
			}
		}
	}
	//===================================================================
	// get product properties
	// ===================================================================	 
	private void getProdProp(Product prod) throws Exception 
	{
		List<ProductProperty> lProps = prod.getProperties();
		if(lProps != null)
		{					
			for(ProductProperty prop : lProps)
			{
				String sKey = prop.getAttributeFQN();
				if(sKey != null)
				{
					List<ProductPropertyValue> lPpValues = prop.getValues();
					if(lPpValues != null)
					{
						for(ProductPropertyValue lppval : lPpValues)
						{
							String sVal = lppval.getValue().toString();									
							if(sKey.indexOf("Tenant~Weight") >= 0){	sWeight = sVal; }
							//System.out.println(sKey + " = " + sVal);
						}	
					}
				}
			}
		}
	}
	//===================================================================
	// check if image added to product setup 
	// ===================================================================	 
	private String getImgAvail(Product prod)
	{
		String sImg = "N";
		String sImgDocNm = "NONE";
		List<ProductLocalizedImage> lProdImg = prod.getContent().getProductImages();
		if(lProdImg != null)
		{
			for(ProductLocalizedImage img : lProdImg)
			{
				String ImgId = img.getCmsId();
				if(ImgId != null )
				{ 
					sImg = "Y"; 
				    sImgDocNm = getDocument(ImgId);
				}				 
			}
		}	

		return sImg;
	}
	
	//===================================================================
	// get document properties 
	// //03/10/2015 - run with error Not authorized to perform 'DocumentList/GetDocument'
	// // need Joanne help  
	// ===================================================================	 
	private String getDocument(String cmsId) {
		String sImage = "NONE";
        try {
            DocumentResource resource = new DocumentResource(apiContext);
            Document doc = resource.getDocument("files@mozu", cmsId);
            sImage = doc.getName();
            
            // for test only
            //System.out.println(" docProp=" + doc.getListFQN() + doc.getName());
            //ObjectMapper mapper = new ObjectMapper();
            //System.out.println(mapper.writerWithDefaultPrettyPrinter()
            //.writeValueAsString(doc));
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return sImage;
    }

	
	
	//===================================================================
	// get product inventory
	//===================================================================
    private void getInventory(String code) throws Exception {
        LocationResource lResource = new LocationResource(apiContext);
        LocationCollection lcollection = lResource.getLocations();
        List<Location> llist = lcollection.getItems();
        for (Location location : llist) {
            try {            	
                LocationInventoryResource resource = new LocationInventoryResource(apiContext);
                
                for(String sloc : sLoc)
                {
                	try {
                	LocationInventory inventory = resource.getLocationInventory(code, sloc);
                   
                	vChild.add(code);                
                	vChildLoc.add(sloc);
                	String sqty = inventory.getStockOnHand().toString(); 
                	vChildInv.add(sqty);
                	System.out.println(code + " " + sloc + " " + sqty);
                	} catch (Exception e) {  }
                }
                
            } catch (Exception e) {            	
            	//System.out.println(e.getMessage());
            	e.printStackTrace();
            }
            
           
        }
    }	
	
	/**
	 * ===================================================================
	 * test
	 * ===================================================================	 */
	public static void main(String[] args) 
	{
		MozuProducts mprod = new MozuProducts();
		try {
			//mprod.getProducts();
			String sFilter = "ProductCode eq 4004059971500";
			System.out.println("sssss");
			mprod.getProductsByFilter(sFilter);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
