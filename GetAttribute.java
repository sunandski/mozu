package com.test.api;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.io.*;
import java.math.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mozu.api.contracts.commerceruntime.discounts.AppliedLineItemProductDiscount;
import com.mozu.api.contracts.commerceruntime.fulfillment.Shipment;
import com.mozu.api.contracts.commerceruntime.payments.Payment;
import com.mozu.api.contracts.location.Location;
import com.mozu.api.contracts.location.LocationCollection;
import com.mozu.api.contracts.productadmin.ProductOption;
import com.mozu.api.contracts.content.Document;
import com.mozu.api.contracts.productadmin.Attribute;
import com.mozu.api.contracts.productadmin.AttributeInProductType;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValue;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueInProductType;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueLocalizedContent;
import com.mozu.api.contracts.productadmin.LocationInventory;
import com.mozu.api.contracts.productadmin.LocationInventoryCollection;
import com.mozu.api.contracts.productadmin.Product;
import com.mozu.api.contracts.productadmin.ProductCategory;
import com.mozu.api.contracts.productadmin.ProductCollection;
import com.mozu.api.contracts.productadmin.ProductInCatalogInfo;
import com.mozu.api.contracts.productadmin.ProductInventoryInfo;
import com.mozu.api.contracts.productadmin.ProductLocalizedImage;
import com.mozu.api.contracts.productadmin.ProductOptionValue;
import com.mozu.api.contracts.productadmin.ProductProperty;
import com.mozu.api.contracts.productadmin.ProductPropertyValue;
import com.mozu.api.contracts.productadmin.ProductPublishingInfo;
import com.mozu.api.contracts.productadmin.ProductType;
import com.mozu.api.contracts.productadmin.ProductTypeCollection;
import com.mozu.api.contracts.productadmin.ProductVariation;
import com.mozu.api.contracts.productadmin.ProductVariationPagedCollection;
import com.mozu.api.contracts.productruntime.ProductImage;
import com.mozu.api.resources.commerce.admin.LocationResource;
import com.mozu.api.resources.commerce.catalog.admin.attributedefinition.*;
import com.mozu.api.resources.content.documentlists.DocumentResource;
import com.mozu.api.ApiContext;
import com.mozu.api.MozuApiContext;
import com.mozu.api.resources.commerce.catalog.admin.products.LocationInventoryResource;
import com.mozu.api.resources.commerce.catalog.admin.products.ProductVariationResource;


public class GetAttribute extends MozuInit
{
	private ApiContext apiContext;

	public GetAttribute() 
	{
		try 
		{  
			System.out.println("MozuProducts init();");
			init();
			apiContext = super.getApiCont();
			System.out.println(123);
			
			/* *********** get attribute */
			AttributeResource attres = new AttributeResource(apiContext);		
			Attribute attr = attres.getAttribute("tenant~shoe-color");
			
			String sAttrCode = attr.getAttributeCode();
			
			
			ProductTypeResource prodType = new ProductTypeResource(apiContext);
			ProductTypeCollection prodcol = prodType.getProductTypes(0,1,null, "Name eq 'Test Shoe'", null);
			List<ProductType>  lprodty = prodcol.getItems();
			for(ProductType ptype : lprodty)
			{
				// print first product if any				
				
				AttributeInProductType aptOpt = new AttributeInProductType();
				aptOpt.setAttributeFQN(attr.getAttributeFQN());
				aptOpt.setIsInheritedFromBaseType(false);
				aptOpt.setAttributeDetail(attr);
				aptOpt.setIsHiddenProperty(false);
				aptOpt.setIsMultiValueProperty(false);
				aptOpt.setIsRequiredByAdmin(false);
				aptOpt.setOrder(0);
				
				AttributeVocabularyValueLocalizedContent vocValLocolized = new AttributeVocabularyValueLocalizedContent();				
				vocValLocolized.setStringValue("red");
				vocValLocolized.setLocaleCode("en-US");
				
				AttributeVocabularyValue vocValLoc = new AttributeVocabularyValue();
				vocValLoc.setValue("red");
				vocValLoc.setValueSequence(2736);
                vocValLoc.setContent(vocValLocolized); 
                
                AttributeVocabularyValueInProductType vocVal = new AttributeVocabularyValueInProductType();
				vocVal.setValue("red");
				vocVal.setOrder(0);
				vocVal.setVocabularyValueDetail(vocValLoc);
				
				List<AttributeVocabularyValueInProductType> lvocval  =  new ArrayList<AttributeVocabularyValueInProductType>();
				lvocval.add(vocVal);
				aptOpt.setVocabularyValues(lvocval);
				
				//ptype.getName();
				List<AttributeInProductType> opProdTyp = ptype.getOptions();
				
				opProdTyp.add(aptOpt);		
				
				ptype.setOptions(opProdTyp);
				
				
				
				List<AttributeInProductType> tOpt =  ptype.getOptions();
				for(AttributeInProductType a : tOpt)
				{
					System.out.println("opt");
				}
				prodType.updateProductType(ptype, ptype.getId());
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
	}
	
	

	public static void main(String[] args) {
		GetAttribute getattr = new GetAttribute();

	}

}
