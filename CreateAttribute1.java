package com.test.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.io.*;
import java.math.*;

import com.mozu.api.contracts.productadmin.Attribute;
import com.mozu.api.contracts.productadmin.AttributeInProductType;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValue;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueInProductType;
import com.mozu.api.contracts.productadmin.AttributeVocabularyValueLocalizedContent;
import com.mozu.api.contracts.productadmin.ProductType;
import com.mozu.api.contracts.productadmin.ProductTypeCollection;
import com.mozu.api.resources.commerce.catalog.admin.attributedefinition.*;
import com.mozu.api.ApiContext;

/**
 * @author vrozen
 * @ttile Create new attribute in Mozu 
 */
public class CreateAttribute1  extends MozuInit
{
	private ApiContext apiContext = null;
	private AttributeResource attres = null;	
	private Attribute attrib = null;
	private List<AttributeVocabularyValue> lvocval = null;
	private ProductTypeResource prodType = null;
	private ProductTypeCollection prodcol = null;
	private AttributeInProductType aptOpt = null;
	private List<AttributeVocabularyValue> lvocValLoc = null;
	private AttributeVocabularyValue vocVal = null;
	private boolean bError = false;
	private String sError = "NONE";
	
	/** =======================================================
	 * Constructor - connect to mozu
	 * ======================================================= */
	public CreateAttribute1() 
	{
		try {
				apiContext = super.getApiCont();
				attres = new AttributeResource(apiContext);
				prodType = new ProductTypeResource(apiContext);
			} 
		    catch (Exception e)
		    {
		    	System.out.println("\n" + e.getMessage());
		    	bError = true;
		    	sError = e.getMessage();
		    }
	}
	
	//=======================================================
  	// set new attribute
  	//=======================================================
    public void rtvAttr(String sAttr)
    {
    	try 
    	{
    		attrib = attres.getAttribute(sAttr);
    		lvocval  =  new ArrayList<AttributeVocabularyValue>();    		
    	}
    	catch (Exception e)
	    {
	    	System.out.println("\n" + e.getMessage());
	    	bError = true;
	    	sError = e.getMessage();
	    }
    }
	
    //=======================================================
  	// set new attribute
  	//=======================================================
    public void setNewAttr(String sAttr, String sId, String sValue)
    {
    	// set new attribute
    	try {	
    		
    		AttributeVocabularyValueLocalizedContent vocValLocolized = new AttributeVocabularyValueLocalizedContent();				
    		vocValLocolized.setStringValue(sValue);
    		vocValLocolized.setLocaleCode("en-US");
    		
    		
    		AttributeVocabularyValue vocValLoc = new AttributeVocabularyValue();
    		vocValLoc.setValue(sId);
    		vocValLoc.setContent(vocValLocolized);
    		
    		lvocValLoc = attrib.getVocabularyValues();
    		lvocValLoc.add(vocValLoc);
    		
    		attrib.setVocabularyValues(lvocValLoc);    		
    	} 
    	catch (Exception e) 
    	{
    		System.out.println("\n" + e.getMessage());
	    	bError = true;
	    	sError = e.getMessage();
		}	
    }
    //=======================================================
    // update attributes resources  
  	//=======================================================
    public void updAttr(String sAttr)
    {
    	try
    	{
    		attres.updateAttribute(attrib, sAttr);
    	}
    	catch (Exception e) 
    	{
    		System.out.println("\n" + e.getMessage());
	    	bError = true;
	    	sError = e.getMessage();
		}
    }
    //=======================================================
    // return error status 
    //=======================================================
    public boolean isInError(){return bError; }
    public String getError(){return sError; }
    
	/** =======================================================
	 * test
	 * ======================================================= */
	public static void main(String[] args) 
	{
		CreateAttribute1 crtattr = new CreateAttribute1();
		String sAttr = "Tenant~Color";
		
		crtattr.rtvAttr(sAttr);
		
		crtattr.setNewAttr(sAttr, "1", "Red_001");
		crtattr.setNewAttr(sAttr, "2", "Red_002");
		crtattr.setNewAttr(sAttr, "3", "Red_003");
		
		crtattr.updAttr(sAttr);
	}

}
